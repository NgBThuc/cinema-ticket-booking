import { useMediaQuery } from '@mantine/hooks';
import { NotificationsProvider } from '@mantine/notifications';
import { useSelector } from 'react-redux';
import { Navigate, Route, Routes } from 'react-router-dom';
import AdminLayout from './components/layouts/Admin/AdminLayout';
import PageLayout from './components/layouts/Page/PageLayout';
import AdminMoviesAddPage from './pages/Admin/Movies/AdminMoviesAddPage';
import AdminMoviesEditPage from './pages/Admin/Movies/AdminMoviesEditPage';
import AdminMoviesPage from './pages/Admin/Movies/AdminMoviesPage';
import AdminMovieSchedulePage from './pages/Admin/Schedule/AdminMovieSchedulePage';
import AdminUsersAddPage from './pages/Admin/Users/AdminUsersAddPage';
import AdminUsersEditPage from './pages/Admin/Users/AdminUsersEditPage';
import AdminUsersPage from './pages/Admin/Users/AdminUsersPage';
import BookingPage from './pages/Booking';
import Error404Page from './pages/Error/Error404Page';
import Error500Page from './pages/Error/Error500Page';
import HomePage from './pages/Home';
import LoginPage from './pages/Login';
import MovieDetailPage from './pages/MovieDetail';
import ProfilePage from './pages/Profile';
import RegisterPage from './pages/Register';
import ScrollToTop from './pages/ScrollToTop';
import { RootState } from './store';

const App = () => {
  const isLoggedIn = useSelector((state: RootState) => state.auth.isLoggedIn);
  const isAdmin = useSelector(
    (state: RootState) => state.auth.user?.maLoaiNguoiDung === 'QuanTri'
  );
  const isSmallerThanMd = useMediaQuery('(max-width: 992px)');

  return (
    <NotificationsProvider
      limit={5}
      position={isSmallerThanMd ? 'top-center' : 'bottom-right'}
      transitionDuration={0}
    >
      <Routes>
        <Route element={<PageLayout />}>
          <Route path='/' element={<HomePage />} />
          <Route element={<ScrollToTop />}>
            <Route path='/movies/:movieId' element={<MovieDetailPage />} />
            <Route path='/booking/:scheduleId' element={<BookingPage />} />
            <Route
              path='/profile'
              element={
                isLoggedIn ? (
                  <ProfilePage />
                ) : (
                  <Navigate replace to={'/login'} />
                )
              }
            />
          </Route>
        </Route>
        <Route
          path='/login'
          element={isLoggedIn ? <Navigate replace to={'/'} /> : <LoginPage />}
        />
        <Route
          path='/register'
          element={
            isLoggedIn ? <Navigate replace to={'/'} /> : <RegisterPage />
          }
        />
        <Route
          path='/admin'
          element={isAdmin ? <AdminLayout /> : <Navigate replace to={'/'} />}
        >
          <Route path='/admin/users' element={<AdminUsersPage />} />
          <Route path='/admin/users/add' element={<AdminUsersAddPage />} />
          <Route
            path='/admin/users/edit/:username'
            element={<AdminUsersEditPage />}
          />
          <Route path='/admin/movies' element={<AdminMoviesPage />} />
          <Route path='/admin/movies/add' element={<AdminMoviesAddPage />} />
          <Route
            path='/admin/movies/edit/:movieId'
            element={<AdminMoviesEditPage />}
          />
          <Route
            path='/admin/movies/schedule/:movieId'
            element={<AdminMovieSchedulePage />}
          />
        </Route>
        <Route path='/500' element={<Error500Page />}></Route>
        <Route path='*' element={<Error404Page />}></Route>
      </Routes>
    </NotificationsProvider>
  );
};

export default App;
