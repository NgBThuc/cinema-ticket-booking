import { TheaterGroupInfo, TheaterSystemInfo } from '../types/apis/theaterInfo';
import { ScheduleData } from '../types/forms/adminMoviesForm';
import { https } from './configURL';

export const theaterServices = {
  getTheaterSystemInfo: async (): Promise<TheaterSystemInfo[]> => {
    let response = await https.get('/api/QuanLyRap/LayThongTinHeThongRap');
    return response.data.content;
  },
  getTheaterGroupInfo: async (
    theaterSystemId: string
  ): Promise<TheaterGroupInfo[]> => {
    let response = await https.get(
      '/api/QuanLyRap/LayThongTinCumRapTheoHeThong',
      {
        params: {
          maHeThongRap: theaterSystemId,
        },
      }
    );
    return response.data.content;
  },
  createMovieSchedule: (scheduleData: ScheduleData) => {
    return https.post('/api/QuanLyDatVe/TaoLichChieu', scheduleData);
  },
};
