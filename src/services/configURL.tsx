import axios from 'axios';
import { API_TOKEN, BASE_URL } from '../constant/constant';
import { localServices } from './localService';

export const https = axios.create({
  baseURL: BASE_URL,
  headers: {
    TokenCybersoft: API_TOKEN,
  },
});

https.interceptors.request.use(function (config) {
  const needBearerTokenUrl = [
    '/api/QuanLyDatVe/DatVe',
    '/api/QuanLyNguoiDung/ThongTinTaiKhoan',
    '/api/QuanLyNguoiDung/ThemNguoiDung',
    '/api/QuanLyNguoiDung/CapNhatThongTinNguoiDung',
    '/api/QuanLyNguoiDung/LayThongTinNguoiDung',
    '/api/QuanLyNguoiDung/XoaNguoiDung',
    '/api/QuanLyPhim/XoaPhim',
    '/api/QuanLyPhim/CapNhatPhimUpload',
    '/api/QuanLyDatVe/TaoLichChieu',
  ];

  if (
    config.url &&
    needBearerTokenUrl.includes(config.url) &&
    (config.method === 'post' || config.method === 'delete')
  ) {
    config.headers = {
      ...config.headers,
      Authorization: 'Bearer ' + localServices.user.get()?.accessToken,
    };
  }
  return config;
});
