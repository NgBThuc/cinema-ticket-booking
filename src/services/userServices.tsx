import { GROUP_ID } from '../constant/constant';
import { UserAdminInfo } from '../types/apis/userAdminInfo';
import { UserInfo } from '../types/apis/userInfo';
import { UserInteract } from '../types/apis/userInteract';
import { UsersListItem } from '../types/apis/userList';
import { LoginData } from '../types/apis/userLogin';
import { UserRegister } from '../types/apis/userRegister';
import { https } from './configURL';

export const userServices = {
  login: (loginData: LoginData) => {
    return https.post('/api/QuanLyNguoiDung/DangNhap', loginData);
  },
  register: (registerData: UserRegister) => {
    return https.post('/api/QuanLyNguoiDung/DangKy', registerData);
  },
  getInfo: async (): Promise<UserInfo> => {
    let response = await https.post('/api/QuanLyNguoiDung/ThongTinTaiKhoan');
    return response.data.content;
  },
  getUserList: async (): Promise<UsersListItem[]> => {
    let response = await https.get(
      `/api/QuanLyNguoiDung/LayDanhSachNguoiDung?MaNhom=${GROUP_ID}`
    );
    return response.data.content;
  },
  addNewUser: (newUserData: UserInteract) => {
    return https.post('/api/QuanLyNguoiDung/ThemNguoiDung', newUserData);
  },
  updateUser: (updateUserData: UserInteract) => {
    return https.post(
      '/api/QuanLyNguoiDung/CapNhatThongTinNguoiDung',
      updateUserData
    );
  },
  getUserAdminInfo: async (username: string): Promise<UserAdminInfo> => {
    let response = await https.post(
      `/api/QuanLyNguoiDung/LayThongTinNguoiDung`,
      null,
      {
        params: {
          taiKhoan: username,
        },
      }
    );
    return response.data.content;
  },
  deleteUser: (username: string) => {
    return https.delete('/api/QuanLyNguoiDung/XoaNguoiDung', {
      params: {
        TaiKhoan: username,
      },
    });
  },
};
