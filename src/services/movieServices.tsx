import { GROUP_ID } from '../constant/constant';
import { BannerList } from '../types/apis/bannerList';
import { MovieInformation } from '../types/apis/movieInformation';
import { MovieSchedule } from '../types/apis/movieSchedule';
import { MovieItem } from '../types/apis/moviesList';
import { TheaterSchedule } from '../types/apis/theaterSchedule';
import { https } from './configURL';

export const movieServices = {
  getBannerList: async (): Promise<BannerList> => {
    let response = await https.get('/api/QuanLyPhim/LayDanhSachBanner');
    return response.data.content;
  },
  getMovieInformation: async (movieId: string): Promise<MovieInformation> => {
    let response = await https.get(
      `/api/QuanLyPhim/LayThongTinPhim?MaPhim=${movieId}`
    );
    return response.data.content;
  },
  getMoviesList: async (): Promise<MovieItem[]> => {
    let response = await https.get(
      `/api/QuanLyPhim/LayDanhSachPhim?maNhom=${GROUP_ID}`
    );
    return response.data.content;
  },
  getMovieSchedule: async (movieId: string): Promise<MovieSchedule> => {
    let response = await https.get(
      `/api/QuanLyRap/LayThongTinLichChieuPhim?MaPhim=${movieId}`
    );
    return response.data.content;
  },
  getMovieScheduleByTheater: async (
    theaterSystemId?: string
  ): Promise<TheaterSchedule> => {
    let response = await https.get(
      `/api/QuanLyRap/LayThongTinLichChieuHeThongRap?maNhom=${GROUP_ID}${
        theaterSystemId ? `&maHeThongRap=${theaterSystemId}` : ''
      }`
    );
    return response.data.content;
  },
  deleteMovie: (movieId: number) => {
    return https.delete('/api/QuanLyPhim/XoaPhim', {
      params: {
        MaPhim: movieId,
      },
    });
  },
  addNewMovie: (newMovieData: FormData) => {
    return https.post('/api/QuanLyPhim/ThemPhimUploadHinh', newMovieData);
  },
  updateMovie: (movieUpdateData: FormData) => {
    return https.post('/api/QuanLyPhim/CapNhatPhimUpload', movieUpdateData);
  },
};
