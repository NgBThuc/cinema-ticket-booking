import { LoginResponse } from '../types/apis/userLogin';

export const localServices = {
  user: {
    get: (): LoginResponse | null => {
      let userJsonData = localStorage.getItem('USER');
      if (userJsonData) {
        return JSON.parse(userJsonData);
      } else {
        return null;
      }
    },
    set: (loginResponse: LoginResponse): void => {
      localStorage.setItem('USER', JSON.stringify(loginResponse));
    },
    remove: (): void => {
      localStorage.removeItem('USER');
    },
  },
};
