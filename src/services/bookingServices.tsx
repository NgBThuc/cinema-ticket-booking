import { Booking } from '../types/apis/booking';
import { BookingList } from '../types/apis/bookingList';
import { https } from './configURL';

export const bookingServices = {
  confirm: (bookingData: Booking) => {
    return https.post('/api/QuanLyDatVe/DatVe', bookingData);
  },
  getBookingList: async (bookingId: string): Promise<BookingList> => {
    let response = await https.get(
      `api/QuanLyDatVe/LayDanhSachPhongVe?MaLichChieu=${bookingId}`
    );
    return response.data.content;
  },
};
