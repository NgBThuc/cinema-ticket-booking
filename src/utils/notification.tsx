import { showNotification } from '@mantine/notifications';

export const notification = {
  error: (title: string, message: string) => {
    showNotification({
      title: title,
      message: message,
      autoClose: 5000,
      color: 'red.9',
    });
  },
  success: (title: string, message: string) => {
    showNotification({
      title: title,
      message: message,
      autoClose: 5000,
      color: 'green.5',
    });
  },
  loading: (title: string, message: string) => {
    showNotification({
      title: title,
      message: message,
      autoClose: 5000,
      loading: true,
      disallowClose: true,
      color: 'blue.5',
    });
  },
};
