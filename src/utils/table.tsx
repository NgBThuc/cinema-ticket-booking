import _ from 'lodash';

export const table = {
  filterData: <
    T extends { [key: string]: string | number | boolean | undefined }
  >(
    data: T[],
    search: string
  ) => {
    const searchQuery = search.toLowerCase().trim();
    return data.filter((item) =>
      _.keys(data[0]).some((key) => {
        return item[key]?.toString().toLowerCase().includes(searchQuery);
      })
    );
  },
  sortData: <
    T extends { [key: string]: string | number | boolean | undefined }
  >(
    data: T[],
    payload: {
      sortBy: keyof T | null;
      isReversed: boolean;
      search: string;
    }
  ) => {
    const { sortBy } = payload;

    if (!sortBy) {
      return table.filterData(data, payload.search);
    }

    return table.filterData(
      _.orderBy(
        [...data],
        (usersListItem) => usersListItem[sortBy],
        payload.isReversed ? ['desc'] : ['asc']
      ),
      payload.search
    );
  },
};
