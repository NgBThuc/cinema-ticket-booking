export const formValidate = {
  required(value: string, name: string) {
    return value.trim().length === 0 ? `${name} không được để trống` : null;
  },
  password(value: string) {
    const regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[^\da-zA-Z]).{8,}$/;
    return regex.test(value)
      ? null
      : 'Mật khẩu phải bao gồm ít nhất 8 ký tự trong đó bao gồm ít nhất 1 ký tự viết hoa, 1 ký tự viết thường, 1 chữ số và 1 ký tự đặc biệt.';
  },
  email(value: string) {
    const regex =
      /^(([^<>()[\].,;:\s@"]+(\.[^<>()[\].,;:\s@"]+)*)|(".+"))@(([^<>()[\].,;:\s@"]+\.)+[^<>()[\].,;:\s@"]{2,})$/i;
    return regex.test(value) ? null : 'Vui lòng nhập email hợp lệ';
  },
  phoneNumber(value: string) {
    const regex = /(84|0[3|5|7|8|9])+([0-9]{8})\b/;
    return regex.test(value) ? null : 'Vui lòng nhập số điện thoại hợp lệ';
  },
};
