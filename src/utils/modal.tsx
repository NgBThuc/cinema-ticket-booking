import { Group, Stack, Text } from '@mantine/core';
import { openConfirmModal, openModal } from '@mantine/modals';
import LoginForm from '../components/forms/Login/LoginForm';
import VideoEmbed from '../components/ui/VideoEmbed';
import store from '../store';
import { uiActions } from '../store/uiSlice';

export const openConfirmBookingModal = (
  totalInfoData: string,
  onConfirmHandler?: () => void
) =>
  openConfirmModal({
    modalId: 'confirmBooking',
    centered: true,
    title: (
      <Text color='crusta' size='xl' weight='bold'>
        Xác nhận thanh toán
      </Text>
    ),
    onConfirm: onConfirmHandler,
    labels: { confirm: 'Xác nhận', cancel: 'Hủy bỏ' },
    children: (
      <Stack spacing={'xs'}>
        <Text weight={'bold'}>
          Vui lòng kiểm kỹ thông tin trước khi thanh toán:
        </Text>
        <Group spacing={'xs'}>
          <Text weight='bold'>Tổng số tiền cần thanh toán:</Text>
          <Text weight={'bold'} color='crusta' size='lg'>
            {totalInfoData}
          </Text>
        </Group>
        <Text weight='bold'>Bấm xác nhận để tiến hành thanh toán</Text>
      </Stack>
    ),
  });

export const openRequestLoginModal = () => {
  store.dispatch(uiActions.showLoginModal());
  openModal({
    modalId: 'requestLogin',
    centered: true,
    title: (
      <Text color='crusta' size='xl' weight='bold'>
        Vui lòng đăng nhập để tiến hành đặt vé
      </Text>
    ),
    children: <LoginForm />,
    onClose: () => {
      store.dispatch(uiActions.hideLoginModal());
    },
  });
};

export const openMovieTrailerModal = (
  movieName: string,
  trailerLink: string
) => {
  openModal({
    centered: true,
    title: (
      <Text color='crusta' size='lg' weight='bold'>
        {movieName} Official Trailer
      </Text>
    ),
    children: <VideoEmbed embedLink={trailerLink} />,
    size: 'xl',
  });
};
