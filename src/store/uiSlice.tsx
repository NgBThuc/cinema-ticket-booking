import { createSlice } from '@reduxjs/toolkit';

const initialUiState = {
  isLoginModalShow: false,
  isAdminNavBarShow: false,
};

const uiSlice = createSlice({
  name: 'ui',
  initialState: initialUiState,
  reducers: {
    showLoginModal(state) {
      state.isLoginModalShow = true;
    },
    hideLoginModal(state) {
      state.isLoginModalShow = false;
    },
    toggleAdminNavBar(state) {
      state.isAdminNavBarShow = !state.isAdminNavBarShow;
    },
  },
});

export const uiActions = uiSlice.actions;
export default uiSlice;
