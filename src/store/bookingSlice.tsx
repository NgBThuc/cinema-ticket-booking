import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import _ from 'lodash';

interface BookingInitialState {
  bookingList: { seatId: number; seatPrice: number; seatName: string }[];
}

const initialState: BookingInitialState = {
  bookingList: [],
};

const bookingSlice = createSlice({
  name: 'booking',
  initialState,
  reducers: {
    selectSeatHandler(
      state,
      action: PayloadAction<{
        seatId: number;
        seatPrice: number;
        seatName: string;
      }>
    ) {
      state.bookingList.push(action.payload);
    },
    deselectSeatHandler(state, action: PayloadAction<number>) {
      _.remove(state.bookingList, {
        seatId: action.payload,
      });
    },
    resetBookingList(state) {
      state.bookingList = [];
    },
  },
});

export const bookingActions = bookingSlice.actions;
export default bookingSlice;
