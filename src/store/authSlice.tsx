import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { localServices } from '../services/localService';
import { LoginResponse } from '../types/apis/userLogin';

interface InitialState {
  user: LoginResponse | null;
  isLoggedIn: boolean;
}

const initialState: InitialState = {
  user: localServices.user.get(),
  isLoggedIn: !!localServices.user.get()?.accessToken,
};

const authSlice = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    login(state, action: PayloadAction<LoginResponse>) {
      localServices.user.set(action.payload);
      state.user = action.payload;
      state.isLoggedIn = true;
    },
    logout(state) {
      localServices.user.remove();
      state.user = null;
      state.isLoggedIn = false;
    },
  },
});

export const authActions = authSlice.actions;
export default authSlice;
