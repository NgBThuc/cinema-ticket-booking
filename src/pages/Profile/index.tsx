import Profile from '../../components/profile/Profile';
import ScrollToTopButton from '../../components/ui/ScrollToTopButton';

const ProfilePage = () => {
  return (
    <>
      <Profile />
      <ScrollToTopButton />
    </>
  );
};

export default ProfilePage;
