import Booking from '../../components/booking/Booking';
import ScrollToTopButton from '../../components/ui/ScrollToTopButton';

const BookingPage = () => {
  return (
    <>
      <Booking />
      <ScrollToTopButton />
    </>
  );
};

export default BookingPage;
