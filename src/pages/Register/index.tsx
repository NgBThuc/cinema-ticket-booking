import { Center } from '@mantine/core';
import Register from '../../components/forms/Register/Register';

const RegisterPage = () => {
  return (
    <Center
      sx={(theme) => ({
        height: '100vh',
        minHeight: 800,
        width: '100vw',
        paddingInline: theme.spacing.md,
        paddingBlock: '3rem',
        backgroundColor: theme.colors.mirage[5],
      })}
    >
      <Register />
    </Center>
  );
};

export default RegisterPage;
