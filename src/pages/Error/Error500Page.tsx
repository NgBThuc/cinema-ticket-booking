import Error500 from '../../components/error/Error500';

const Error500Page = () => {
  return (
    <>
      <Error500 />
    </>
  );
};

export default Error500Page;
