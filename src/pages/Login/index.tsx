import { Center } from '@mantine/core';
import Login from '../../components/forms/Login/Login';

const LoginPage = () => {
  return (
    <Center
      sx={(theme) => ({
        height: '100vh',
        minHeight: 800,
        width: '100vw',
        paddingInline: theme.spacing.md,
        paddingBlock: '3rem',
        backgroundColor: theme.colors.mirage[5],
      })}
    >
      <Login />
    </Center>
  );
};

export default LoginPage;
