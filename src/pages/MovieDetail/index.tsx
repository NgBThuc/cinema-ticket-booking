import MovieDetail from '../../components/movies/detail/MovieDetail';
import ScrollToTopButton from '../../components/ui/ScrollToTopButton';

const MovieDetailPage = () => {
  return (
    <>
      <MovieDetail />
      <ScrollToTopButton />
    </>
  );
};

export default MovieDetailPage;
