import { useEffect } from 'react';
import { useLocation } from 'react-router-dom';
import HomePageCarousel from '../../components/carousel/HomePageCarousel';
import MoviesList from '../../components/movies/MoviesList';
import BookingStepper from '../../components/stepper/BookingStepper';
import Theaters from '../../components/theaters/Theaters';
import ScrollToTopButton from '../../components/ui/ScrollToTopButton';

const HomePage = () => {
  const location = useLocation();

  useEffect(() => {
    if (location.hash) {
      let targetElement = document.getElementById(location.hash.slice(1));
      if (targetElement) {
        targetElement.scrollIntoView({ behavior: 'smooth' });
      }
    } else {
      window.scrollTo({ top: 0, left: 0, behavior: 'smooth' });
    }
  }, [location.hash]);

  return (
    <>
      <HomePageCarousel />
      <BookingStepper />
      <MoviesList />
      <Theaters />
      <ScrollToTopButton />
    </>
  );
};

export default HomePage;
