import { Button, createStyles, Title } from '@mantine/core';
import { Link } from 'react-router-dom';
import AdminMoviesTable from '../../../components/admin/Movies/AdminMoviesTable';

const useStyles = createStyles((theme) => ({
  addNewMoviesButton: {
    alignSelf: 'center',
  },
}));

const AdminMoviesPage = () => {
  const { classes } = useStyles();

  return (
    <>
      <Title order={2} color='crusta' align='center' transform='uppercase'>
        Quản lý phim
      </Title>
      <Button
        component={Link}
        to='/admin/movies/add'
        className={classes.addNewMoviesButton}
      >
        Thêm phim mới
      </Button>
      <AdminMoviesTable />
    </>
  );
};

export default AdminMoviesPage;
