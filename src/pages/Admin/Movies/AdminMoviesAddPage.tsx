import { Title } from '@mantine/core';
import { cleanNotifications } from '@mantine/notifications';
import { useMutation, useQueryClient } from '@tanstack/react-query';
import { AxiosError } from 'axios';
import dayjs from 'dayjs';
import { useNavigate } from 'react-router-dom';
import AdminMoviesForm from '../../../components/forms/Admin/Movies/AdminMoviesForm';
import { movieServices } from '../../../services/movieServices';
import { AdminMoviesFormValues } from '../../../types/forms/adminMoviesForm';
import { notification } from '../../../utils/notification';

const AdminMoviesAddPage = () => {
  const queryClient = useQueryClient();
  const navigate = useNavigate();
  const addNewMovie = useMutation(
    (newMovieData: FormData) => {
      return movieServices.addNewMovie(newMovieData);
    },
    {
      onSuccess: () => {
        cleanNotifications();
        notification.success(
          'Thêm phim thành công',
          'Dữ liệu phim mới đã được thêm vào cơ sở dữ liệu'
        );
      },
      onError: (error: AxiosError<{ content: string }>) => {
        cleanNotifications();
        let errorData = error.response?.data;
        let errorMessage = errorData?.content;
        if (errorMessage) {
          notification.error('Thêm phim không thành công', errorMessage);
        }
      },
    }
  );

  if (addNewMovie.isLoading) {
    notification.loading('Thực hiện thêm phim', 'Vui lòng đợi trong giây lát');
  }

  const formSubmitHandler = (formValues: AdminMoviesFormValues) => {
    let formData = new FormData();
    formData.append('tenPhim', formValues.tenPhim);
    formData.append('moTa', formValues.moTa);
    formData.append(
      'ngayKhoiChieu',
      dayjs(formValues.ngayKhoiChieu).format('DD/MM/YYYY')
    );
    formData.append('trailer', formValues.trailer);
    formData.append('maNhom', formValues.maNhom);
    formData.append('sapChieu', formValues.sapChieu.toString());
    formData.append('dangChieu', formValues.dangChieu.toString());
    formData.append('hot', formValues.hot.toString());
    formData.append('danhGia', formValues.danhGia.toString());
    if (formValues.hinhAnh && typeof formValues.hinhAnh !== 'string') {
      formData.append('File', formValues.hinhAnh, formValues.hinhAnh.name);
    }
    addNewMovie.mutate(formData, {
      onSuccess: () => {
        queryClient.invalidateQueries(['moviesList']);
        navigate('/admin/movies');
      },
    });
  };

  return (
    <>
      <>
        <Title order={2} color='crusta' transform='uppercase' align='center'>
          Thêm phim mới
        </Title>
        <AdminMoviesForm usage='add' formSubmitHandler={formSubmitHandler} />
      </>
    </>
  );
};

export default AdminMoviesAddPage;
