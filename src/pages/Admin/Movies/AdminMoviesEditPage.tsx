import { Box, Loader, Title } from '@mantine/core';
import { cleanNotifications } from '@mantine/notifications';
import { useMutation, useQueryClient } from '@tanstack/react-query';
import { AxiosError } from 'axios';
import dayjs from 'dayjs';
import { useNavigate, useParams } from 'react-router-dom';
import AdminMoviesForm from '../../../components/forms/Admin/Movies/AdminMoviesForm';
import { GROUP_ID } from '../../../constant/constant';
import { useMovieInformation } from '../../../hooks/useMovieInformation';
import { movieServices } from '../../../services/movieServices';
import { AdminMoviesFormValues } from '../../../types/forms/adminMoviesForm';
import { notification } from '../../../utils/notification';

const AdminMoviesEditPage = () => {
  const queryClient = useQueryClient();
  const navigate = useNavigate();
  const params = useParams<{ movieId: string }>();
  const movieInfoQuery = useMovieInformation(params.movieId!);
  const updateMovie = useMutation(
    (movieUpdateData: FormData) => {
      return movieServices.updateMovie(movieUpdateData);
    },
    {
      onSuccess: () => {
        cleanNotifications();
        notification.success(
          'Cập nhật phim thành công',
          'Dữ liệu phim cập nhật đã được ghi nhận vào cơ sở dữ liệu'
        );
      },
      onError: (error: AxiosError<{ content: string }>) => {
        cleanNotifications();
        let errorData = error.response?.data;
        let errorMessage = errorData?.content;
        if (errorMessage) {
          notification.error('Cập nhật phim không thành công', errorMessage);
        }
      },
    }
  );

  if (updateMovie.isLoading) {
    notification.loading(
      'Thực hiện cập nhật thông tin phim',
      'Vui lòng đợi trong giây lát'
    );
  }

  const initialFormValues: AdminMoviesFormValues = {
    tenPhim: movieInfoQuery.data?.tenPhim || '',
    trailer: movieInfoQuery.data?.trailer || '',
    moTa: movieInfoQuery.data?.moTa || '',
    maNhom: GROUP_ID,
    ngayKhoiChieu: movieInfoQuery.data?.ngayKhoiChieu
      ? new Date(movieInfoQuery.data.ngayKhoiChieu)
      : new Date(),
    sapChieu: movieInfoQuery.data?.sapChieu || false,
    dangChieu: movieInfoQuery.data?.dangChieu || false,
    hot: movieInfoQuery.data?.hot || false,
    danhGia: movieInfoQuery.data?.danhGia || 0,
    hinhAnh: movieInfoQuery.data?.hinhAnh || null,
  };

  const moviesFormSubmitHandler = (formValues: AdminMoviesFormValues) => {
    let formData = new FormData();
    formData.append('maPhim', params.movieId!);
    formData.append('tenPhim', formValues.tenPhim);
    formData.append('moTa', formValues.moTa);
    formData.append(
      'ngayKhoiChieu',
      dayjs(formValues.ngayKhoiChieu).format('DD/MM/YYYY')
    );
    formData.append('trailer', formValues.trailer);
    formData.append('maNhom', formValues.maNhom);
    formData.append('sapChieu', formValues.sapChieu.toString());
    formData.append('dangChieu', formValues.dangChieu.toString());
    formData.append('hot', formValues.hot.toString());
    formData.append('danhGia', formValues.danhGia.toString());
    if (formValues.hinhAnh && typeof formValues.hinhAnh !== 'string') {
      formData.append('File', formValues.hinhAnh, formValues.hinhAnh.name);
    }
    updateMovie.mutate(formData, {
      onSuccess: () => {
        queryClient.invalidateQueries(['moviesList']);
        navigate('/admin/movies');
        movieInfoQuery.refetch();
      },
    });
  };

  return (
    <>
      <Title order={2} color='crusta' transform='uppercase' align='center'>
        Cập nhật thông tin phim
      </Title>
      <Box
        sx={(theme) => ({
          width: '100%',
          position: 'relative',
          paddingBlock: theme.spacing.md,
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
        })}
      >
        {movieInfoQuery.isLoading && <Loader />}
        {movieInfoQuery.data && (
          <AdminMoviesForm
            usage='edit'
            initialFormValues={initialFormValues}
            formSubmitHandler={moviesFormSubmitHandler}
          />
        )}
      </Box>
    </>
  );
};

export default AdminMoviesEditPage;
