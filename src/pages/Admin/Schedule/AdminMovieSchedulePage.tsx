import { Loader, Stack, Title } from '@mantine/core';
import { useParams } from 'react-router-dom';
import AdminSchedulesForm from '../../../components/forms/Admin/Schedule/AdminSchedulesForm';
import { useMovieInformation } from '../../../hooks/useMovieInformation';

const AdminMovieSchedulePage = () => {
  const params = useParams<{ movieId: string }>();
  const movieInformationQuery = useMovieInformation(params.movieId!);

  return (
    <>
      <Title order={2} color='crusta' transform='uppercase' align='center'>
        Tạo lịch chiếu phim
      </Title>
      {movieInformationQuery.isLoading && <Loader mx='auto' />}
      {movieInformationQuery.data && (
        <>
          <Stack>
            <Title order={3} color='crusta.0' align='center'>
              {movieInformationQuery.data?.tenPhim}
            </Title>
          </Stack>
          <AdminSchedulesForm />
        </>
      )}
    </>
  );
};

export default AdminMovieSchedulePage;
