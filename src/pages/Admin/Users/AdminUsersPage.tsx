import { Button, createStyles, Title } from '@mantine/core';
import { Link } from 'react-router-dom';
import AdminUsersTable from '../../../components/admin/Users/AdminUsersTable';

const useStyles = createStyles((theme) => ({
  addNewUserButton: {
    alignSelf: 'center',
  },
}));

const AdminUsersPage = () => {
  const { classes } = useStyles();

  return (
    <>
      <Title order={2} color='crusta' align='center' transform='uppercase'>
        Quản Lý Người Dùng
      </Title>
      <Button
        component={Link}
        to='/admin/users/add'
        className={classes.addNewUserButton}
      >
        Thêm người dùng mới
      </Button>
      <AdminUsersTable />
    </>
  );
};

export default AdminUsersPage;
