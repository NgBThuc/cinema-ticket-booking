import { Title } from '@mantine/core';
import { cleanNotifications } from '@mantine/notifications';
import { useMutation, useQueryClient } from '@tanstack/react-query';
import { AxiosError } from 'axios';
import { useNavigate } from 'react-router-dom';
import AdminUserForm from '../../../components/forms/Admin/Users/AdminUserForm';
import { GROUP_ID } from '../../../constant/constant';
import { userServices } from '../../../services/userServices';
import { UserInteract } from '../../../types/apis/userInteract';
import { notification } from '../../../utils/notification';

const AdminUsersAddPage = () => {
  const queryClient = useQueryClient();
  const navigate = useNavigate();
  const addNewUser = useMutation(
    (newUserData: UserInteract) => {
      return userServices.addNewUser(newUserData);
    },
    {
      onSuccess: () => {
        cleanNotifications();
        notification.success(
          'Thêm người dùng thành công',
          'Thông tin người dùng đã được thêm vào cơ sở dữ liệu'
        );
      },
      onError: (error: AxiosError<{ content: string }>) => {
        cleanNotifications();
        let errorData = error.response?.data;
        let errorMessage = errorData?.content;
        if (errorMessage) {
          notification.error('Thêm người dùng không thành công', errorMessage);
        }
      },
    }
  );

  if (addNewUser.isLoading) {
    notification.loading(
      'Thực hiện thêm người dùng',
      'Vui lòng đợi trong giây lát'
    );
  }

  const initialUserFormValues = {
    taiKhoan: '',
    matKhau: '',
    email: '',
    soDt: '',
    maNhom: GROUP_ID,
    maLoaiNguoiDung: '',
    hoTen: '',
  };

  const adminUserFormSubmitHandler = (formValues: UserInteract) => {
    addNewUser.mutate(formValues, {
      onSuccess: () => {
        queryClient.invalidateQueries(['usersList']);
        navigate('/admin/users');
      },
    });
  };

  return (
    <>
      <Title order={2} color='crusta' transform='uppercase' align='center'>
        Thêm người dùng mới
      </Title>
      <AdminUserForm
        formSubmitHandler={adminUserFormSubmitHandler}
        initialFormValues={initialUserFormValues}
        usage='add'
      />
    </>
  );
};

export default AdminUsersAddPage;
