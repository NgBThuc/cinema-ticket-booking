import { Box, Loader, Title } from '@mantine/core';
import { cleanNotifications } from '@mantine/notifications';
import { useMutation, useQueryClient } from '@tanstack/react-query';
import { AxiosError } from 'axios';
import { useNavigate, useParams } from 'react-router-dom';
import AdminUserForm from '../../../components/forms/Admin/Users/AdminUserForm';
import { useUserAdminInformation } from '../../../hooks/useUserAdminInformation';
import { userServices } from '../../../services/userServices';
import { UserInteract } from '../../../types/apis/userInteract';
import { notification } from '../../../utils/notification';

const AdminUsersEditPage = () => {
  const queryClient = useQueryClient();
  const navigate = useNavigate();
  const updateUser = useMutation(
    (updateUserData: UserInteract) => {
      return userServices.updateUser(updateUserData);
    },
    {
      onSuccess: () => {
        cleanNotifications();
        notification.success(
          'Cập nhật người dùng thành công',
          'Thông tin thay đổi đã được ghi nhận vào cơ sở dữ liệu'
        );
      },
      onError: (error: AxiosError<{ content: string }>) => {
        cleanNotifications();
        let errorData = error.response?.data;
        let errorMessage = errorData?.content;
        if (errorMessage) {
          notification.error(
            'Cập nhật người dùng không thành công',
            errorMessage
          );
        }
      },
    }
  );
  const params = useParams<{ username: string }>();
  const userAdminInfoQuery = useUserAdminInformation(params.username!);

  if (updateUser.isLoading) {
    notification.loading(
      'Thực hiện cập nhật thông tin người dùng',
      'Vui lòng đợi trong giây lát'
    );
  }

  const initialUserFormValues = {
    taiKhoan: userAdminInfoQuery.data?.taiKhoan || '',
    matKhau: userAdminInfoQuery.data?.matKhau || '',
    email: userAdminInfoQuery.data?.email || '',
    soDt: userAdminInfoQuery.data?.soDT || '',
    maNhom: userAdminInfoQuery.data?.maNhom || '',
    maLoaiNguoiDung: userAdminInfoQuery.data?.maLoaiNguoiDung || '',
    hoTen: userAdminInfoQuery.data?.hoTen || '',
  };

  const userFormSubmitHandler = (formValues: UserInteract) => {
    updateUser.mutate(formValues, {
      onSuccess: () => {
        queryClient.invalidateQueries(['usersList']);
        navigate('/admin/users');
        userAdminInfoQuery.refetch();
      },
    });
  };

  return (
    <>
      <Title order={2} color='crusta' transform='uppercase' align='center'>
        Cập nhật người dùng
      </Title>
      <Box
        sx={(theme) => ({
          width: '100%',
          position: 'relative',
          paddingBlock: theme.spacing.md,
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
        })}
      >
        {userAdminInfoQuery.isLoading && <Loader />}
        {userAdminInfoQuery.data && (
          <AdminUserForm
            formSubmitHandler={userFormSubmitHandler}
            initialFormValues={initialUserFormValues}
            usage='edit'
          />
        )}
      </Box>
    </>
  );
};

export default AdminUsersEditPage;
