import { createStyles, Group, Stack } from '@mantine/core';
import { Outlet } from 'react-router-dom';
import AdminHeader from '../../admin/AdminHeader';
import AdminNavBar from '../../admin/AdminNavBar';

const useStyles = createStyles((theme) => ({
  adminContent: {
    paddingInline: theme.spacing.md,
    paddingBlock: theme.spacing.xl,
    display: 'flex',
    flexDirection: 'column',
    gap: theme.spacing.lg,
    minWidth: 0,
    overflowY: 'auto',
    justifyContent: 'start',
    height: 'calc(100vh - 70px)',
    minHeight: 800,

    [theme.fn.smallerThan('md')]: {
      marginBlock: theme.spacing.xl,
      justifyContent: 'start',
      minHeight: 0,
    },
  },
}));

const AdminLayout = () => {
  const { classes } = useStyles();

  return (
    <Stack spacing={0}>
      <AdminHeader />
      <Group
        sx={(theme) => ({
          minWidth: 0,
        })}
        spacing={0}
        noWrap={true}
      >
        <AdminNavBar />
        <div style={{ flexGrow: 1 }} className={classes.adminContent}>
          <Outlet />
        </div>
      </Group>
    </Stack>
  );
};

export default AdminLayout;
