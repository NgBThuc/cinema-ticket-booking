import { Outlet } from 'react-router-dom';
import PageFooter from '../Footer/PageFooter';
import PageHeader from '../Header/PageHeader';

const PageLayout = () => {
  return (
    <>
      <PageHeader />
      <Outlet />
      <PageFooter />
    </>
  );
};

export default PageLayout;
