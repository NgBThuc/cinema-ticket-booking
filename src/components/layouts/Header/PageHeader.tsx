import { Anchor, Burger, createStyles, Group, Header } from '@mantine/core';
import { useState } from 'react';
import { Link } from 'react-router-dom';
import BrandLogo from '../../ui/BrandLogo';
import HeaderButtonGroup from './HeaderButtonGroup';
import HeaderDrawer from './HeaderDrawer';
import HeaderLinks from './HeaderLinks';

const useStyles = createStyles((theme) => ({
  header: {
    backgroundColor: theme.colors.mirage[2],
    borderBottom: `2xp solid ${theme.colors.lynch[3]}`,
    boxShadow: theme.shadows.lg,
  },

  links: {
    [theme.fn.smallerThan('md')]: {
      display: 'none',
    },
  },

  buttonGroup: {
    [theme.fn.smallerThan('md')]: {
      display: 'none',
    },
  },

  burger: {
    [theme.fn.largerThan('md')]: {
      display: 'none',
    },
  },
}));

const PageHeader = () => {
  const { classes } = useStyles();
  const [isOpened, setIsOpened] = useState<boolean>(false);

  const burgerClickHandler = () => {
    setIsOpened(true);
  };

  const closeDrawerHandler = () => {
    setIsOpened(false);
  };

  return (
    <Header height={100} className={classes.header}>
      <Group position={'apart'} py={'lg'} className={'container'}>
        <Anchor component={Link} to='/' onClick={closeDrawerHandler}>
          <BrandLogo />
        </Anchor>

        <Group className={classes.links} spacing={'xl'}>
          <HeaderLinks />
        </Group>

        <Group className={classes.buttonGroup} spacing={'xs'}>
          <HeaderButtonGroup />
        </Group>

        <Burger
          className={classes.burger}
          opened={isOpened}
          onClick={burgerClickHandler}
          style={{ visibility: `${isOpened ? 'hidden' : 'visible'}` }}
        />
      </Group>

      <HeaderDrawer
        isOpened={isOpened}
        closeDrawerHandler={closeDrawerHandler}
      />
    </Header>
  );
};

export default PageHeader;
