import { Button, Divider } from '@mantine/core';
import { IconEdit, IconLogout, IconUser, IconUsers } from '@tabler/icons';
import { useSelector } from 'react-redux';
import { Link, useLocation, useNavigate } from 'react-router-dom';
import { RootState, useAppDispatch } from '../../../store';
import { authActions } from '../../../store/authSlice';
import { notification } from '../../../utils/notification';

interface HeaderButtonGroupProps {
  closeDrawerHandler?: () => void;
}

const HeaderButtonGroup = (props: HeaderButtonGroupProps) => {
  const navigate = useNavigate();
  const dispatch = useAppDispatch();
  const isLoggedIn = useSelector((state: RootState) => state.auth.isLoggedIn);
  const isAdmin = useSelector(
    (state: RootState) => state.auth.user?.maLoaiNguoiDung === 'QuanTri'
  );
  const userInformation = useSelector((state: RootState) => state.auth.user);
  const location = useLocation();

  function logoutHandler() {
    dispatch(authActions.logout());
    if (props.closeDrawerHandler) {
      props.closeDrawerHandler();
    }
    notification.success(
      'Đăng xuất tài khoản thành công',
      'Xin chào và hẹn gặp lại'
    );
    navigate('/', { replace: true });
  }

  if (isLoggedIn) {
    return (
      <>
        <Button
          radius='md'
          leftIcon={<IconLogout />}
          variant='filled'
          onClick={logoutHandler}
        >
          Đăng Xuất
        </Button>
        <Button
          component={Link}
          to='/profile'
          radius={'md'}
          leftIcon={<IconUser />}
          variant={'outline'}
          onClick={props.closeDrawerHandler}
        >
          {userInformation?.hoTen}
        </Button>
        {isAdmin && (
          <Button
            component={Link}
            to='/admin/users'
            radius='md'
            leftIcon={<IconEdit />}
            variant={'outline'}
            onClick={props.closeDrawerHandler}
          >
            Quản trị
          </Button>
        )}
      </>
    );
  }

  return (
    <>
      <Button
        component={Link}
        to='/login'
        state={location.pathname}
        radius={'md'}
        leftIcon={<IconUsers />}
        variant={'filled'}
        onClick={props.closeDrawerHandler}
      >
        Đăng Nhập
      </Button>
      <Divider
        orientation='vertical'
        sx={(theme) => ({
          borderColor: theme.colors.crusta[3],
          [theme.fn.smallerThan('md')]: {
            display: 'none',
          },
        })}
      ></Divider>
      <Button
        component={Link}
        to='/register'
        state={location.pathname}
        radius={'md'}
        leftIcon={<IconUsers />}
        variant={'outline'}
        onClick={props.closeDrawerHandler}
      >
        Đăng Ký
      </Button>
    </>
  );
};

export default HeaderButtonGroup;
