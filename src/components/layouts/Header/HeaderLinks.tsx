import { Anchor } from '@mantine/core';
import { Link } from 'react-router-dom';

interface HeaderLinksProps {
  closeDrawerHandler?: () => void;
}

const HeaderLinks = (props: HeaderLinksProps) => {
  return (
    <>
      <Anchor
        component={Link}
        to='/#bookingStepper'
        onClick={props.closeDrawerHandler}
      >
        Đặt vé
      </Anchor>
      <Anchor
        component={Link}
        to='/#moviesList'
        onClick={props.closeDrawerHandler}
      >
        Phim Đang Chiếu
      </Anchor>
      <Anchor
        component={Link}
        to='/#theaters'
        onClick={props.closeDrawerHandler}
      >
        Cụm Rạp
      </Anchor>
    </>
  );
};

export default HeaderLinks;
