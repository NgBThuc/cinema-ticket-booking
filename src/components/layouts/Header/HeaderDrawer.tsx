import { Drawer, Stack } from '@mantine/core';
import { useMediaQuery } from '@mantine/hooks';
import { FC } from 'react';
import HeaderButtonGroup from './HeaderButtonGroup';
import HeaderLinks from './HeaderLinks';

interface HeaderDrawerProps {
  isOpened: boolean;
  closeDrawerHandler: () => void;
}

const HeaderDrawer: FC<HeaderDrawerProps> = (props) => {
  const isMobileScreen = useMediaQuery('(max-width: 768px)');

  return (
    <Drawer
      opened={props.isOpened}
      onClose={props.closeDrawerHandler}
      size={isMobileScreen ? '100%' : 'lg'}
      padding={'lg'}
      transition={'slide-right'}
      transitionDuration={200}
    >
      <Stack
        align={'center'}
        spacing={'lg'}
        py={'lg'}
        sx={(theme) => ({
          borderBottom: `2px solid ${theme.colors.mirage[3]}`,
        })}
      >
        <HeaderLinks closeDrawerHandler={props.closeDrawerHandler} />
      </Stack>
      <Stack py={'lg'}>
        <HeaderButtonGroup closeDrawerHandler={props.closeDrawerHandler} />
      </Stack>
    </Drawer>
  );
};

export default HeaderDrawer;
