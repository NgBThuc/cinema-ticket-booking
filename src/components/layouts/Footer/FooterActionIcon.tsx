import { ActionIcon, createStyles, Group } from '@mantine/core';
import {
  IconBrandFacebook,
  IconBrandInstagram,
  IconBrandTwitter,
  IconBrandYoutube,
} from '@tabler/icons';

const useStyles = createStyles((theme) => ({
  brandIcon: {
    color: theme.colors.lynch[0],
    transition: 'color 300ms ease',

    '&:hover': {
      color: theme.colors.crusta[3],
    },
  },
}));

const FooterActionIcon = () => {
  const { classes } = useStyles();

  return (
    <Group position='center'>
      <ActionIcon size={'lg'} radius='lg' color={'crusta'}>
        <IconBrandTwitter className={classes.brandIcon} />
      </ActionIcon>
      <ActionIcon size={'lg'} radius='lg' color={'crusta'}>
        <IconBrandYoutube className={classes.brandIcon} />
      </ActionIcon>
      <ActionIcon size={'lg'} radius='lg' color={'crusta'}>
        <IconBrandInstagram className={classes.brandIcon} />
      </ActionIcon>
      <ActionIcon size={'lg'} radius='lg' color={'crusta'}>
        <IconBrandFacebook className={classes.brandIcon} />
      </ActionIcon>
    </Group>
  );
};

export default FooterActionIcon;
