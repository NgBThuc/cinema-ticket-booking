import { Anchor, createStyles, Stack } from '@mantine/core';

const useStyles = createStyles((theme) => ({
  navLinks: {
    [theme.fn.largerThan('md')]: {
      flexDirection: 'row',
    },
  },

  anchor: {
    color: theme.colors.lynch[0],
    fontWeight: 'normal',
  },
}));

const FooterNavLink = () => {
  const { classes } = useStyles();

  return (
    <Stack className={classes.navLinks} align={'center'}>
      <Anchor className={classes.anchor}>Về chúng tôi</Anchor>
      <Anchor className={classes.anchor}>Liên hệ</Anchor>
      <Anchor className={classes.anchor}>Chính sách bảo mật</Anchor>
      <Anchor className={classes.anchor}>Tuyển dụng</Anchor>
    </Stack>
  );
};

export default FooterNavLink;
