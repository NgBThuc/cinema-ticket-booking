import { Stack, Text } from '@mantine/core';
import FooterActionIcon from './FooterActionIcon';

const FooterCopy = () => {
  return (
    <Stack
      sx={(theme) => ({
        flexDirection: 'column',
        gap: theme.spacing.lg,

        [theme.fn.largerThan('md')]: {
          flexDirection: 'row',
          justifyContent: 'space-between',
        },
      })}
    >
      <Text align='center' sx={(theme) => ({ color: theme.colors.lynch[0] })}>
        &#169; 2022 NgBT. Đã đăng ký bản quyền
      </Text>
      <FooterActionIcon />
    </Stack>
  );
};

export default FooterCopy;
