import { createStyles, Group, Stack, Text, Title } from '@mantine/core';
import BrandLogo from '../../ui/BrandLogo';

const useStyles = createStyles((theme) => ({
  logo: {
    [theme.fn.largerThan('md')]: {
      alignItems: 'start',
    },
  },
}));

const FooterLogo = () => {
  const { classes } = useStyles();

  return (
    <Stack className={classes.logo}>
      <Group position='center'>
        <BrandLogo />
        <Title order={3} color='crusta'>
          CineTick
        </Title>
      </Group>
      <Text align='center' sx={(theme) => ({ color: theme.colors.lynch[0] })}>
        Đặt phim nhanh chóng và tiện lợi
      </Text>
    </Stack>
  );
};

export default FooterLogo;
