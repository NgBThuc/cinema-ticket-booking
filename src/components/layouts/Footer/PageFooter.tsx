import { Box, Divider, Stack } from '@mantine/core';
import FooterCopy from './FooterCopy';
import FooterLogo from './FooterLogo';
import FooterNavLink from './FooterNavLink';

const PageFooter = () => {
  return (
    <Box
      component='footer'
      sx={(theme) => ({
        paddingBlock: '3rem',
        backgroundColor: theme.colors.mirage[6],
      })}
    >
      <Stack className={'container'}>
        <Stack
          justify={'center'}
          align='center'
          sx={(theme) => ({
            [theme.fn.largerThan('md')]: {
              flexDirection: 'row',
              justifyContent: 'space-between',
            },
          })}
        >
          <FooterLogo />
          <Divider orientation='horizontal' />
          <FooterNavLink />
        </Stack>
        <Divider orientation='horizontal' />
        <FooterCopy />
      </Stack>
    </Box>
  );
};

export default PageFooter;
