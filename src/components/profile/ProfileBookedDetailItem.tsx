import { Group, Text } from '@mantine/core';

interface ProfileBookedDetailItemProps {
  title: string;
  data: string;
}

const ProfileBookedDetailItem = (props: ProfileBookedDetailItemProps) => {
  return (
    <Group spacing='xs'>
      <Text size='md' color='crusta' weight='bold'>
        {props.title}
        {': '}
      </Text>
      <Text size='md' weight='bold'>
        {props.data}
      </Text>
    </Group>
  );
};

export default ProfileBookedDetailItem;
