import { Title } from '@mantine/core';

const ProfileTitle = () => {
  return (
    <Title order={1} variant='gradient' transform='uppercase' align='center'>
      Thông tin người dùng
    </Title>
  );
};

export default ProfileTitle;
