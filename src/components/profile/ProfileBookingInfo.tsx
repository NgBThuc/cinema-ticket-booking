import { Accordion, Spoiler, Stack, Title } from '@mantine/core';
import _ from 'lodash';
import { useUserInformation } from '../../hooks/useUserInformation';
import Loading from '../ui/Loading';
import ProfileBookedDetail from './ProfileBookedDetail';
import ProfileBookedMovie from './ProfileBookedMovie';

const ProfileBookingInfo = () => {
  const userInfoQuery = useUserInformation();

  if (userInfoQuery.isLoading) {
    return <Loading />;
  }

  return (
    <Stack>
      <Title
        order={2}
        color='crusta'
        variant='gradient'
        align='center'
        transform='uppercase'
      >
        Thông tin đặt vé
      </Title>

      <Accordion>
        <Spoiler
          maxHeight={450}
          showLabel='Mở rộng'
          hideLabel='Thu gọn'
          styles={(theme) => ({
            control: {
              marginLeft: theme.spacing.md,
              marginBlock: theme.spacing.md,
              color: theme.colors.crusta[3],
              textDecoration: 'underline',
              textUnderlineOffset: '5px',

              '&:hover, &:focus': {
                color: theme.colors.lynch[0],
                textDecoration: 'underline',
                textUnderlineOffset: '5px',
              },
            },
          })}
        >
          {userInfoQuery.data &&
            _.reverse(userInfoQuery.data.thongTinDatVe).map((bookedMovie) => {
              return (
                <Accordion.Item
                  key={bookedMovie.maVe}
                  value={bookedMovie.maVe.toString()}
                >
                  <Accordion.Control
                    sx={(theme) => ({
                      '&:hover, &:focus': {
                        borderTopLeftRadius: theme.radius.md,
                        borderTopRightRadius: theme.radius.md,
                        background: theme.colors.mirage[0],
                      },
                    })}
                  >
                    <ProfileBookedMovie
                      movieName={bookedMovie.tenPhim}
                      bookedDate={bookedMovie.ngayDat}
                    />
                  </Accordion.Control>
                  <Accordion.Panel>
                    <ProfileBookedDetail detailData={bookedMovie} />
                  </Accordion.Panel>
                </Accordion.Item>
              );
            })}
        </Spoiler>
      </Accordion>
    </Stack>
  );
};

export default ProfileBookingInfo;
