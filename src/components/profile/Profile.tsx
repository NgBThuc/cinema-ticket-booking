import { Box, Stack } from '@mantine/core';
import ProfileBookingInfo from './ProfileBookingInfo';
import ProfileInfo from './ProfileInfo';
import ProfileTitle from './ProfileTitle';

const Profile = () => {
  return (
    <Box
      sx={{
        paddingBlock: '5rem',
      }}
    >
      <Stack className='container' spacing='xl'>
        <ProfileTitle />
        <ProfileInfo />
        <ProfileBookingInfo />
      </Stack>
    </Box>
  );
};

export default Profile;
