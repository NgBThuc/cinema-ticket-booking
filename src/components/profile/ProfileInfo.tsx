import { Avatar, Grid, Stack, Text } from '@mantine/core';
import { useUserInformation } from '../../hooks/useUserInformation';
import Loading from '../ui/Loading';

const ProfileInfo = () => {
  const userInfoQuery = useUserInformation();

  if (userInfoQuery.isLoading) {
    return <Loading />;
  }

  return (
    <Stack
      align='center'
      sx={(theme) => ({
        [theme.fn.largerThan('md')]: {
          flexDirection: 'row',
          width: 'max-content',
          marginInline: 'auto',
          gap: '3rem',
        },
      })}
    >
      <Avatar size={200} src={null} radius='xl' color='crusta.0'></Avatar>
      <Grid
        columns={12}
        sx={(theme) => ({
          width: '100%',
          maxWidth: '500px',
          marginInline: 'auto',

          '& > div': {
            overflowWrap: 'break-word',
          },

          '& > div:nth-of-type(2n + 1)': {
            color: theme.colors.crusta[3],
            fontWeight: 'bold',
          },

          '& > div:nth-of-type(2n + 2)': {
            fontWeight: 'bold',
          },
        })}
      >
        <Grid.Col span={6}>
          <Text>Họ và tên:</Text>
        </Grid.Col>
        <Grid.Col span={6}>
          <Text>{userInfoQuery.data?.hoTen}</Text>
        </Grid.Col>
        <Grid.Col span={6}>
          <Text>Email:</Text>
        </Grid.Col>
        <Grid.Col span={6}>
          <Text>{userInfoQuery.data?.email}</Text>
        </Grid.Col>
        <Grid.Col span={6}>
          <Text>Số điện thoại:</Text>
        </Grid.Col>
        <Grid.Col span={6}>
          <Text>{userInfoQuery.data?.soDT}</Text>
        </Grid.Col>
      </Grid>
    </Stack>
  );
};

export default ProfileInfo;
