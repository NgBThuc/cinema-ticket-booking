import { Stack, Text } from '@mantine/core';
import dayjs from 'dayjs';

interface ProfileBookedMovieProps {
  movieName: string;
  bookedDate: string;
}

const ProfileBookedMovie = (props: ProfileBookedMovieProps) => {
  return (
    <Stack spacing='xs'>
      <Text color='lynch.0' weight='bold' transform='uppercase'>
        {props.movieName}
      </Text>
      <Text size='md' italic={true} color='crusta.0' weight='bold'>
        {dayjs(props.bookedDate).format('DD/MM/YYYY - HH:mm')}
      </Text>
    </Stack>
  );
};

export default ProfileBookedMovie;
