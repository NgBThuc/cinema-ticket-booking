import { Stack } from '@mantine/core';
import { BookingInfo } from '../../types/apis/userInfo';
import ProfileBookedDetailItem from './ProfileBookedDetailItem';

interface ProfileBookedDetailProps {
  detailData: BookingInfo;
}

const ProfileBookedDetail = (props: ProfileBookedDetailProps) => {
  const { detailData } = props;

  const profileBookedDetailData = [
    {
      title: 'Tổng số tiền đã thanh toán',
      data: new Intl.NumberFormat('vi-VN').format(
        detailData.giaVe * detailData.danhSachGhe.length
      ),
    },
    {
      title: 'Địa điểm',
      data: detailData.danhSachGhe[0].tenHeThongRap,
    },
    {
      title: 'Tên Rạp',
      data: detailData.danhSachGhe[0].tenRap,
    },
    {
      title: 'Số ghế',
      data: detailData.danhSachGhe
        .map((seatItem) => seatItem.tenGhe)
        .join(', '),
    },
  ];

  return (
    <Stack spacing='sm'>
      {profileBookedDetailData.map((dataItem) => {
        return (
          <ProfileBookedDetailItem
            key={dataItem.title}
            title={dataItem.title}
            data={dataItem.data}
          />
        );
      })}
    </Stack>
  );
};

export default ProfileBookedDetail;
