import { createStyles, Stack, Tabs, Text } from '@mantine/core';
import { useMediaQuery } from '@mantine/hooks';
import { useTheatersSchedule } from '../../hooks/useTheatersSchedule';
import { TheaterGroup } from '../../types/apis/theaterSchedule';
import { TABS_HEIGHT } from './Theaters';
import TheaterSchedule from './TheaterSchedule';

const useStyles = createStyles((theme) => ({
  text: {
    display: 'inline-block',
  },

  tabs: {
    [theme.fn.smallerThan('md')]: {
      display: 'flex',
      flexDirection: 'column',
    },
  },

  tabsList: {
    gap: theme.spacing.lg,
    flexWrap: 'nowrap',
    height: TABS_HEIGHT,
    paddingInline: theme.spacing.md,
    overflowX: 'hidden',
    overflowY: 'scroll',
    borderRightColor: 'transparent',

    [theme.fn.smallerThan('md')]: {
      height: 'auto',
      paddingBlock: theme.spacing.md,
      paddingInline: 0,
      overflowX: 'scroll',
      overflowY: 'hidden',
      minHeight: 0,
    },
  },

  tabsTab: {
    transition: 'all 200ms ease',
    opacity: 0.5,
    borderRadius: 0,
    width: 300,

    '&[data-active]': {
      borderColor: 'transparent',
      borderRadius: theme.radius.md,
      opacity: 1,
      backgroundColor: theme.colors.mirage[6],
    },

    '&:hover, &[data-active]:hover': {
      borderColor: 'transparent',
    },

    [theme.fn.smallerThan('md')]: {
      width: 250,
    },
  },

  tabLabel: {
    width: '100%',
  },

  tabPanel: {
    paddingInline: theme.spacing.md,
    display: 'flex',
    flexDirection: 'column',
    gap: theme.spacing.md,
    height: TABS_HEIGHT,
    overflowY: 'scroll',

    [theme.fn.smallerThan('md')]: {
      paddingBlock: theme.spacing.md,
      paddingInline: 0,
      minHeight: 0,
      height: 'auto',
      overflow: 'scroll',
    },
  },

  button: {
    alignSelf: 'flex-start',
  },
}));

const TheaterGroupDetail = () => {
  const { classes } = useStyles();
  const isSmallerMd = useMediaQuery('(max-width: 992px)');
  const theaterScheduleQuery = useTheatersSchedule();

  const theaterBranchTabContent = (theater: TheaterGroup) => {
    return theater.lstCumRap.map((theaterBranch) => {
      return (
        <Tabs.Tab value={theaterBranch.maCumRap} key={theaterBranch.maCumRap}>
          <Stack spacing={'sm'}>
            <Text
              lineClamp={1}
              className={classes.text}
              color='crusta'
              weight='bold'
            >
              {theaterBranch.tenCumRap}
            </Text>
            <Text className={classes.text} lineClamp={1}>
              {theaterBranch.diaChi}
            </Text>
          </Stack>
        </Tabs.Tab>
      );
    });
  };

  const theaterDetailsTabContent = theaterScheduleQuery.data?.map((theater) => {
    return (
      <Tabs.Panel key={theater.maHeThongRap} value={theater.maHeThongRap}>
        <Tabs
          orientation={isSmallerMd ? 'horizontal' : 'vertical'}
          radius={'sm'}
          defaultValue={theater.lstCumRap[0].maCumRap}
          classNames={{
            root: classes.tabs,
            tabLabel: classes.tabLabel,
            tab: classes.tabsTab,
            tabsList: classes.tabsList,
            panel: classes.tabPanel,
          }}
        >
          <Tabs.List className='custom-scrollbar'>
            {theaterBranchTabContent(theater)}
          </Tabs.List>
          <TheaterSchedule theaterBranch={theater.lstCumRap} />
        </Tabs>
      </Tabs.Panel>
    );
  });

  return <>{theaterDetailsTabContent}</>;
};

export default TheaterGroupDetail;
