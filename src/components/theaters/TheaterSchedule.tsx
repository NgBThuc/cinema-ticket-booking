import {
  Anchor,
  Badge,
  createStyles,
  Grid,
  Group,
  Image,
  Stack,
  Tabs,
  Text,
} from '@mantine/core';
import dayjs from 'dayjs';
import { FC } from 'react';
import { Link } from 'react-router-dom';

interface TheaterScheduleProps {
  theaterBranch: Array<{
    maCumRap: string;
    danhSachPhim: Array<{
      hinhAnh: string;
      tenPhim: string;
      maPhim: number;
      lstLichChieuTheoPhim: Array<{
        ngayChieuGioChieu: string;
        maLichChieu: number;
      }>;
    }>;
  }>;
}

const useStyles = createStyles((theme) => ({
  group: {
    minHeight: 225,
    height: 225,
    borderRadius: theme.radius.md,
    overflowY: 'hidden',
    backgroundColor: theme.colors.mirage[6],

    [theme.fn.smallerThan('sm')]: {
      flexDirection: 'column',
      minHeight: 400,
    },
  },

  imageContainer: {
    width: '35%',
    height: '100%',

    [theme.fn.smallerThan('sm')]: {
      width: '100%',
      height: 150,
    },
  },

  stack: {
    width: '60%',
    alignSelf: 'start',
    height: '100%',
    paddingInline: theme.spacing.md,

    [theme.fn.smallerThan('sm')]: {
      width: '100%',
      textAlign: 'center',
      minHeight: 0,
    },
  },

  badge: {
    padding: theme.spacing.md,
    backgroundColor: theme.colors.crusta[6],
    color: '#ffffff',
    transition: 'background-color 300ms ease',
    cursor: 'pointer',

    '&:hover': {
      backgroundColor: theme.colors.crusta[3],
    },
  },

  grid: {
    margin: 0,
    gap: theme.spacing.md,
    overflowY: 'auto',
    overflowX: 'hidden',
  },

  gridCol: {
    flexBasis: 'auto',
    padding: 0,

    [theme.fn.smallerThan('sm')]: {
      flexBasis: '100%',
      maxWidth: '100%',
      textAlign: 'center',
    },
  },
}));

const TheaterSchedule: FC<TheaterScheduleProps> = (props) => {
  const { classes } = useStyles();

  const theaterScheduleContent = props.theaterBranch.map((theaterBranch) => {
    return (
      <Tabs.Panel
        className='custom-scrollbar'
        key={theaterBranch.maCumRap}
        value={theaterBranch.maCumRap}
      >
        {theaterBranch.danhSachPhim.map((movie) => {
          return (
            <Group
              key={movie.maPhim}
              className={classes.group}
              position='left'
              p='md'
              spacing={'lg'}
              noWrap={true}
            >
              <div className={classes.imageContainer}>
                <Image
                  fit='cover'
                  radius={'md'}
                  src={movie.hinhAnh}
                  height='100%'
                />
              </div>
              <Stack className={classes.stack}>
                <Anchor
                  component={Link}
                  to={`/movies/${movie.maPhim}`}
                  size={'lg'}
                  weight={'bold'}
                >
                  {movie.tenPhim}
                </Anchor>
                <Grid
                  className={`${classes.grid} custom-scrollbar`}
                  columns={12}
                >
                  {movie.lstLichChieuTheoPhim.map((schedule) => {
                    return (
                      <Grid.Col
                        key={schedule.maLichChieu}
                        className={classes.gridCol}
                        span={6}
                      >
                        <Badge
                          component={Link}
                          to={`/booking/${schedule.maLichChieu}`}
                          className={classes.badge}
                        >
                          <Group noWrap={true}>
                            <Text>
                              {dayjs(schedule.ngayChieuGioChieu).format(
                                'DD/MM/YYYY'
                              )}
                            </Text>
                            <Text>
                              {dayjs(schedule.ngayChieuGioChieu).format(
                                'HH:mm'
                              )}
                            </Text>
                          </Group>
                        </Badge>
                      </Grid.Col>
                    );
                  })}
                </Grid>
              </Stack>
            </Group>
          );
        })}
      </Tabs.Panel>
    );
  });

  return <>{theaterScheduleContent}</>;
};

export default TheaterSchedule;
