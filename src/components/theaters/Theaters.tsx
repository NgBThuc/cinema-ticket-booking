import { Box, createStyles, Image, Tabs } from '@mantine/core';
import { useMediaQuery } from '@mantine/hooks';
import { useTheatersSchedule } from '../../hooks/useTheatersSchedule';
import Loading from '../ui/Loading';
import TheaterGroupDetail from './TheaterGroupDetail';

export const TABS_HEIGHT = 600;

const useStyles = createStyles((theme) => ({
  section: {
    height: 'auto',
    background: theme.colors.mirage[2],
    paddingBlock: theme.spacing.xl,
  },

  container: {
    marginBlock: theme.spacing.xl,
  },

  text: {
    display: 'inline-block',
  },

  tabs: {
    marginInline: 'auto',

    [theme.fn.smallerThan('md')]: {
      display: 'flex',
      flexDirection: 'column',
    },
  },

  tabsList: {
    gap: theme.spacing.lg,
    flexWrap: 'nowrap',
    height: TABS_HEIGHT,
    paddingInline: theme.spacing.md,
    overflowX: 'hidden',
    overflowY: 'scroll',
    borderRightColor: 'transparent',

    [theme.fn.smallerThan('md')]: {
      height: 100,
      paddingBlock: theme.spacing.md,
      paddingInline: 0,
      overflowX: 'auto',
      overflowY: 'hidden',
      minHeight: 0,
    },
  },

  tabsTab: {
    transition: 'all 200ms ease',
    opacity: 0.5,
    borderRadius: 0,

    '&[data-active]': {
      borderColor: 'transparent',
      borderRadius: theme.radius.md,
      opacity: 1,
      backgroundColor: theme.colors.mirage[6],
    },

    '&:hover, &[data-active]:hover': {
      borderColor: 'transparent',
    },
  },

  tabLabel: {
    width: '100%',
  },

  tabPanel: {
    minHeight: 0,
    overflow: 'hidden',
  },

  button: {
    alignSelf: 'flex-start',
  },
}));

const Theaters = () => {
  const { classes } = useStyles();
  const isSmallerMd = useMediaQuery('(max-width: 992px)');
  const theaterScheduleQuery = useTheatersSchedule();

  if (theaterScheduleQuery.isLoading) {
    return <Loading />;
  }

  const theatersTabContent = theaterScheduleQuery.data?.map((theater) => {
    return (
      <Tabs.Tab key={theater.maHeThongRap} value={theater.maHeThongRap}>
        <Image src={theater.logo} width={50} height={50} />
      </Tabs.Tab>
    );
  });

  return (
    <Box component='section' className={classes.section} id='theaters'>
      <div className={`container + ${classes.container}`}>
        <Tabs
          orientation={isSmallerMd ? 'horizontal' : 'vertical'}
          radius={'sm'}
          defaultValue={theaterScheduleQuery.data?.[0].maHeThongRap}
          classNames={{
            root: classes.tabs,
            tabsList: classes.tabsList,
            tab: classes.tabsTab,
            panel: classes.tabPanel,
          }}
        >
          <Tabs.List className={'custom-scrollbar'}>
            {theatersTabContent}
          </Tabs.List>
          <TheaterGroupDetail />
        </Tabs>
      </div>
    </Box>
  );
};

export default Theaters;
