import {
  Box,
  Button,
  createStyles,
  FileInput,
  Image,
  NumberInput,
  Stack,
  Switch,
  TextInput,
} from '@mantine/core';
import { DatePicker } from '@mantine/dates';
import { useForm } from '@mantine/form';
import { IconCalendar, IconUpload } from '@tabler/icons';
import 'dayjs/locale/vi';
import { Link } from 'react-router-dom';
import { GROUP_ID } from '../../../../constant/constant';
import { AdminMoviesFormValues } from '../../../../types/forms/adminMoviesForm';
import { formValidate } from '../../../../utils/validate';

const useStyles = createStyles((theme) => ({
  form: {
    display: 'flex',
    flexDirection: 'column',
    gap: theme.spacing.lg,
    maxWidth: 500,
    width: '100%',
    marginInline: 'auto',
  },
}));

const initialFormValues: AdminMoviesFormValues = {
  tenPhim: '',
  trailer: '',
  moTa: '',
  maNhom: GROUP_ID,
  ngayKhoiChieu: new Date(),
  sapChieu: false,
  dangChieu: false,
  hot: false,
  danhGia: 0,
  hinhAnh: null,
};

interface AdminMoviesFormProps {
  usage: 'add' | 'edit';
  initialFormValues?: AdminMoviesFormValues;
  formSubmitHandler: (formValues: AdminMoviesFormValues) => void;
}

const AdminMoviesForm = (props: AdminMoviesFormProps) => {
  const { classes } = useStyles();
  const form = useForm({
    initialValues: props.initialFormValues
      ? props.initialFormValues
      : initialFormValues,
    validateInputOnBlur: true,
    validate: {
      tenPhim: (values) => formValidate.required(values, 'Tên phim'),
      trailer: (values) => formValidate.required(values, 'Trailer link'),
      moTa: (values) => formValidate.required(values, 'Mô tả'),
      ngayKhoiChieu: (values) =>
        formValidate.required(values.toString(), 'Ngày khởi chiếu'),
      danhGia: (values) => {
        if (values === 0) {
          return 'Vui lòng nhập đánh giá cho phim';
        } else {
          return null;
        }
      },
      hinhAnh: (values: File | null | string) => {
        if (values) {
          return null;
        } else {
          return 'Vui lòng chọn poster cho phim';
        }
      },
    },
  });

  return (
    <form
      className={classes.form}
      onSubmit={form.onSubmit(props.formSubmitHandler)}
    >
      <TextInput
        withAsterisk
        label='Tên phim'
        placeholder='Nhập tên phim'
        {...form.getInputProps('tenPhim')}
      />
      <TextInput
        withAsterisk
        label='Trailer link'
        placeholder='Nhập trailer youtube link'
        {...form.getInputProps('trailer')}
      />
      <TextInput
        withAsterisk
        label='Mô tả'
        placeholder='Nhập mô tả'
        {...form.getInputProps('moTa')}
      />
      <DatePicker
        withAsterisk
        locale='vi'
        icon={<IconCalendar />}
        dropdownType='modal'
        label='Ngày khởi chiếu'
        placeholder='Chọn ngày khởi chiếu'
        {...form.getInputProps('ngayKhoiChieu')}
      />
      <Stack
        sx={(theme) => ({
          [theme.fn.largerThan('md')]: {
            flexDirection: 'row',
          },
        })}
      >
        <Switch
          label='Đang chiếu'
          checked={form.values.dangChieu}
          {...form.getInputProps('dangChieu')}
        />
        <Switch
          label='Sắp chiếu'
          checked={form.values.sapChieu}
          {...form.getInputProps('sapChieu')}
        />
        <Switch
          label='Hot'
          checked={form.values.hot}
          {...form.getInputProps('hot')}
        />
      </Stack>
      <NumberInput
        withAsterisk
        label='Đánh giá'
        placeholder='Điểm đánh giá của phim'
        min={0}
        max={10}
        {...form.getInputProps('danhGia')}
      />
      <FileInput
        withAsterisk
        label='Poster phim'
        placeholder='Chọn file hình ảnh cho poster phim (.jpg, .png, .gif)'
        accept='image/jpeg'
        icon={<IconUpload />}
        {...form.getInputProps('hinhAnh')}
      />
      <Box
        sx={(theme) => ({
          border: `2px dashed ${theme.colors.crusta[0]}`,
          width: '100%',
          height: 100,
          borderRadius: theme.radius.md,
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
        })}
      >
        {form.values.hinhAnh && typeof form.values.hinhAnh !== 'string' && (
          <Image
            width=''
            height='80px'
            src={URL.createObjectURL(form.values.hinhAnh)}
          />
        )}
        {form.values.hinhAnh && typeof form.values.hinhAnh === 'string' && (
          <Image width='' height='80px' src={form.values.hinhAnh} />
        )}
      </Box>
      <Button type='submit' color='crusta' variant='filled'>
        {props.usage === 'add' ? 'Thêm phim' : 'Cập nhật phim'}
      </Button>
      <Button
        component={Link}
        to='/admin/movies'
        type='button'
        color='crusta'
        variant='outline'
      >
        Hủy bỏ
      </Button>
    </form>
  );
};

export default AdminMoviesForm;
