import { Button, createStyles, NumberInput, Select } from '@mantine/core';
import { DatePicker, TimeInput } from '@mantine/dates';
import { useForm } from '@mantine/form';
import { cleanNotifications } from '@mantine/notifications';
import {
  IconArmchair,
  IconCalendar,
  IconClock,
  IconDeviceTv,
  IconTicket,
} from '@tabler/icons';
import { useMutation } from '@tanstack/react-query';
import { AxiosError } from 'axios';
import dayjs from 'dayjs';
import 'dayjs/locale/vi';
import { useNavigate, useParams } from 'react-router-dom';
import {
  useTheaterGroupInfo,
  useTheaterSystemInfo,
} from '../../../../hooks/useTheaterInfo';
import { theaterServices } from '../../../../services/theaterServices';
import { ScheduleData } from '../../../../types/forms/adminMoviesForm';
import { AdminScheduleForm } from '../../../../types/forms/adminScheduleForm';
import { notification } from '../../../../utils/notification';
import { formValidate } from '../../../../utils/validate';

const useStyles = createStyles((theme) => ({
  form: {
    width: '100%',
    maxWidth: 500,
    marginInline: 'auto',
    display: 'flex',
    flexDirection: 'column',
    gap: theme.spacing.lg,
  },
}));

const AdminSchedulesForm = () => {
  const { classes } = useStyles();
  const params = useParams<{ movieId: string }>();
  const navigate = useNavigate();
  const theaterSystemInfoQuery = useTheaterSystemInfo();
  const form = useForm<AdminScheduleForm>({
    initialValues: {
      theaterSystem: '',
      theaterGroup: '',
      showDate: new Date(),
      showTime: new Date(),
      ticketPrice: 0,
    },
    validateInputOnBlur: true,
    validate: {
      theaterSystem: (value) => formValidate.required(value, 'Hệ thống rạp'),
      theaterGroup: (value) => formValidate.required(value, 'Cụm rạp'),
      showDate: (value) => (value ? null : 'Ngày chiếu không được để trống'),
      showTime: (value) => (value ? null : 'Giờ chiếu không được để trống'),
      ticketPrice: (value) => (value === 0 ? 'Giá vé không thể bằng 0' : null),
    },
  });
  const theaterGroupInfoQuery = useTheaterGroupInfo(form.values.theaterSystem);
  const createMovieSchedule = useMutation(
    (scheduleData: ScheduleData) => {
      return theaterServices.createMovieSchedule(scheduleData);
    },
    {
      onSuccess: () => {
        cleanNotifications();
        notification.success(
          'Tạo lịch chiếu phim thành công',
          'Lịch chiếu phim đã được ghi nhận vào cơ sở dữ liệu'
        );
      },
      onError: (error: AxiosError<{ content: string }>) => {
        cleanNotifications();
        let errorMessage = error.response?.data.content;
        if (errorMessage) {
          notification.error('Tạo lịch chiếu phim thất bại', errorMessage);
        }
      },
    }
  );

  if (createMovieSchedule.isLoading) {
    notification.loading(
      'Thực hiện tạo lịch chiếu phim',
      'Xin vui lòng đợi trong giây lát'
    );
  }

  const theaterSystemSelectData = theaterSystemInfoQuery.data?.map(
    (theaterSystem) => {
      return {
        value: theaterSystem.maHeThongRap,
        label: theaterSystem.tenHeThongRap.toUpperCase(),
      };
    }
  );

  const theaterGroupSelectData = theaterGroupInfoQuery.data?.map(
    (theaterGroup) => {
      return {
        value: theaterGroup.maCumRap,
        label: theaterGroup.tenCumRap,
      };
    }
  );

  const formSubmitHandler = (formValues: AdminScheduleForm) => {
    const showYear = formValues.showDate.getFullYear();
    const showMonth = formValues.showDate.getMonth();
    const showDate = formValues.showDate.getDate();
    const showHours = formValues.showTime.getHours();
    const showMinutes = formValues.showTime.getMinutes();
    let scheduleData: ScheduleData = {
      maRap: formValues.theaterGroup,
      maPhim: +params.movieId!,
      giaVe: formValues.ticketPrice,
      ngayChieuGioChieu: dayjs(
        new Date(showYear, showMonth, showDate, showHours, showMinutes)
      ).format('DD/MM/YYYY HH:mm:ss'),
    };
    createMovieSchedule.mutate(scheduleData, {
      onSuccess: () => {
        navigate('/admin/movies');
      },
    });
  };

  return (
    <form className={classes.form} onSubmit={form.onSubmit(formSubmitHandler)}>
      <Select
        icon={<IconDeviceTv />}
        withAsterisk
        label='Hệ thống rạp'
        placeholder='Chọn hệ thống rạp'
        data={theaterSystemSelectData || []}
        {...form.getInputProps('theaterSystem')}
      />
      <Select
        icon={<IconArmchair />}
        withAsterisk
        label='Cụm rạp'
        placeholder='Chọn cụm rạp'
        data={theaterGroupSelectData || []}
        {...form.getInputProps('theaterGroup')}
      />
      <DatePicker
        withAsterisk
        locale='vi'
        icon={<IconCalendar />}
        dropdownType='modal'
        label='Ngày chiếu'
        placeholder='Chọn ngày chiếu phim'
        {...form.getInputProps('showDate')}
      />
      <TimeInput
        withAsterisk
        icon={<IconClock />}
        clearable={true}
        label='Giờ chiếu'
        placeholder='Chọn giờ chiếu phim'
        {...form.getInputProps('showTime')}
      />
      <NumberInput
        icon={<IconTicket />}
        label='Giá vé'
        placeholder='Nhập giá vé'
        min={0}
        max={200000}
        {...form.getInputProps('ticketPrice')}
      />
      <Button type='submit' variant='filled' color='crusta'>
        Tạo lịch chiếu
      </Button>
    </form>
  );
};

export default AdminSchedulesForm;
