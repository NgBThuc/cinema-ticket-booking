import {
  Button,
  createStyles,
  PasswordInput,
  Select,
  TextInput,
} from '@mantine/core';
import { useForm } from '@mantine/form';
import {
  IconAlphabetLatin,
  IconAt,
  IconPassword,
  IconPhone,
  IconSignature,
  IconUserCircle,
} from '@tabler/icons';
import { Link } from 'react-router-dom';
import { UserInteract } from '../../../../types/apis/userInteract';
import { formValidate } from '../../../../utils/validate';

const useStyles = createStyles((theme) => ({
  form: {
    marginInline: 'auto',
    maxWidth: 500,
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    gap: theme.spacing.md,
  },
}));

interface AdminUserFormProps {
  usage: 'add' | 'edit';
  initialFormValues: UserInteract;
  formSubmitHandler: (values: UserInteract) => void;
}

const AdminUserForm = (props: AdminUserFormProps) => {
  const { classes } = useStyles();
  const form = useForm({
    initialValues: props.initialFormValues,
    validate: {
      taiKhoan: (value) => formValidate.required(value, 'Tài khoản'),
      matKhau: (value) =>
        props.usage === 'add' ? formValidate.password(value) : null,
      email: (value) => formValidate.email(value),
      soDt: (value) => formValidate.phoneNumber(value),
      maLoaiNguoiDung: (value) =>
        value === '' ? 'Vui lòng chọn ít nhất một loại người dùng' : null,
      hoTen: (value) => formValidate.required(value, 'Họ và tên'),
    },
    validateInputOnBlur: true,
  });

  return (
    <form
      onSubmit={form.onSubmit((values) => {
        props.formSubmitHandler(values);
      })}
      className={classes.form}
    >
      <TextInput
        icon={<IconSignature />}
        withAsterisk
        label='Tài khoản'
        placeholder='Nhập tài khoản của người dùng'
        disabled={props.usage === 'edit' ? true : false}
        {...form.getInputProps('taiKhoan')}
      />
      <PasswordInput
        icon={<IconPassword />}
        withAsterisk
        label='Mật khẩu'
        placeholder='Nhập mật khẩu của người dùng'
        {...form.getInputProps('matKhau')}
      />
      <TextInput
        icon={<IconAlphabetLatin />}
        withAsterisk
        label='Họ và tên'
        placeholder='Nhập họ và tên của người dùng'
        {...form.getInputProps('hoTen')}
      />
      <TextInput
        icon={<IconAt />}
        withAsterisk
        label='Email'
        placeholder='Nhập email của người dùng'
        {...form.getInputProps('email')}
      />
      <TextInput
        icon={<IconPhone />}
        withAsterisk
        label='Số điện thoại'
        placeholder='Nhập số điện thoại của người dùng'
        {...form.getInputProps('soDt')}
      />
      <Select
        icon={<IconUserCircle />}
        withAsterisk
        label='Loại người dùng'
        placeholder='Chọn loại người dùng'
        data={[
          { value: 'KhachHang', label: 'Khách hàng' },
          { value: 'QuanTri', label: 'Quản trị viên' },
        ]}
        {...form.getInputProps('maLoaiNguoiDung')}
      ></Select>
      <Button mt='md' size='md' variant='filled' color='crusta' type='submit'>
        {props.usage === 'add' ? 'Thêm người dùng' : 'Cập nhật người dùng'}
      </Button>
      <Button
        component={Link}
        to='/admin/users'
        size='md'
        variant='outline'
        color='crusta'
        type='button'
      >
        Hủy Bỏ
      </Button>
    </form>
  );
};

export default AdminUserForm;
