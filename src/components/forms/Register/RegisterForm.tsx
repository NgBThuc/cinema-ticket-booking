import { Button, PasswordInput, Stack, TextInput } from '@mantine/core';
import { useForm } from '@mantine/form';
import { cleanNotifications } from '@mantine/notifications';
import {
  IconAt,
  IconPassword,
  IconPhoneCall,
  IconSignature,
  IconUsers,
} from '@tabler/icons';
import { useMutation } from '@tanstack/react-query';
import { AxiosError } from 'axios';
import { Link, useLocation, useNavigate } from 'react-router-dom';
import { GROUP_ID } from '../../../constant/constant';
import { userServices } from '../../../services/userServices';
import { UserRegister } from '../../../types/apis/userRegister';
import { notification } from '../../../utils/notification';
import { formValidate } from '../../../utils/validate';

interface RegisterFormValues {
  taiKhoan: string;
  matKhau: string;
  email: string;
  soDt: string;
  maNhom: string;
  hoTen: string;
}

const formRegisterInitialValues = {
  taiKhoan: '',
  matKhau: '',
  email: '',
  soDt: '',
  maNhom: GROUP_ID,
  hoTen: '',
};

const RegisterForm = () => {
  const navigate = useNavigate();
  const location = useLocation();
  const form = useForm<RegisterFormValues>({
    initialValues: formRegisterInitialValues,
    validate: {
      taiKhoan: (value) => formValidate.required(value, 'Tên đăng nhập'),
      matKhau: (value) => formValidate.password(value),
      email: (value) => formValidate.email(value),
      soDt: (value) => formValidate.phoneNumber(value),
      hoTen: (value) => formValidate.required(value, 'Họ và tên'),
    },
    validateInputOnBlur: true,
  });

  const userRegister = useMutation(
    (registerData: UserRegister) => {
      console.log(registerData);
      return userServices.register(registerData);
    },
    {
      onSuccess: () => {
        cleanNotifications();
        navigate('/login', { replace: true, state: location.state });
        notification.success(
          'Tạo tài khoản thành công',
          'Vui lòng đăng nhập để đặt vé'
        );
      },
      onError: (error: AxiosError<{ content: string }>) => {
        cleanNotifications();
        let errorData = error.response?.data;
        let errorMessage = errorData?.content;
        if (errorMessage) {
          notification.error(
            'Đăng ký tài khoản không thành công',
            errorMessage
          );
        }
      },
    }
  );

  if (userRegister.isLoading) {
    notification.loading(
      'Đang thực hiện đăng ký người dùng mới',
      'Vui lòng đợi trong giây lát'
    );
  }

  function registerSubmitHandler(registerData: UserRegister) {
    userRegister.mutate(registerData);
  }

  return (
    <form onSubmit={form.onSubmit(registerSubmitHandler)}>
      <Stack spacing={'md'}>
        <TextInput
          icon={<IconUsers />}
          label='Tên Đăng Nhập'
          placeholder='Tên Đăng Nhập'
          radius={'md'}
          size={'md'}
          withAsterisk
          {...form.getInputProps('taiKhoan')}
        />
        <PasswordInput
          icon={<IconPassword />}
          label='Mật Khẩu'
          placeholder='Mật Khẩu'
          radius={'md'}
          size='md'
          autoComplete='on'
          withAsterisk
          {...form.getInputProps('matKhau')}
        />
        <TextInput
          icon={<IconSignature />}
          label='Họ và Tên'
          placeholder='Họ và Tên'
          radius={'md'}
          size={'md'}
          withAsterisk
          {...form.getInputProps('hoTen')}
        />
        <TextInput
          icon={<IconAt />}
          label='Email'
          placeholder='Email'
          radius={'md'}
          size={'md'}
          withAsterisk
          {...form.getInputProps('email')}
        />
        <TextInput
          icon={<IconPhoneCall />}
          label='Số Điện Thoại'
          placeholder='Số Điện Thoại'
          radius={'md'}
          size={'md'}
          withAsterisk
          {...form.getInputProps('soDt')}
        />
        <Stack>
          <Button type='submit' variant='filled' mt={'lg'} size={'md'}>
            Đăng Ký
          </Button>
          <Button
            component={Link}
            to={location.state ? location.state : '/'}
            type='button'
            variant='outline'
            size='md'
          >
            Hủy Bỏ
          </Button>
        </Stack>
      </Stack>
    </form>
  );
};

export default RegisterForm;
