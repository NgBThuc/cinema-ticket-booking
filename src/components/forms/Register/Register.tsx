import { Box } from '@mantine/core';
import FormHeader from '../FormHeader';
import RegisterForm from './RegisterForm';

const Register = () => {
  return (
    <Box style={{ maxWidth: 500, width: '100%' }}>
      <FormHeader title='Đăng Ký' subtitile='Đăng ký để tạo tài khoản đặt vé' />
      <RegisterForm />
    </Box>
  );
};

export default Register;
