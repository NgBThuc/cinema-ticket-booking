import { Stack, Text, Title } from '@mantine/core';
import { FC } from 'react';

interface FormHeaderProps {
  title: string;
  subtitile: string;
}

const FormHeader: FC<FormHeaderProps> = (props) => {
  return (
    <Stack style={{ textAlign: 'center' }} mb='xl'>
      <Title order={1} color='crusta'>
        {props.title}
      </Title>
      <Text size={'lg'} color='#ffffff80'>
        {props.subtitile}
      </Text>
    </Stack>
  );
};

export default FormHeader;
