import { Box } from '@mantine/core';
import FormHeader from '../FormHeader';
import LoginForm from './LoginForm';

const Login = () => {
  return (
    <Box
      sx={(theme) => ({
        maxWidth: 600,
        width: '100%',
      })}
    >
      <FormHeader title='Đăng Nhập' subtitile='Đăng nhập để mua vé' />
      <LoginForm />
    </Box>
  );
};

export default Login;
