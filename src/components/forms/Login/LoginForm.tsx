import { Button, PasswordInput, Stack, Text, TextInput } from '@mantine/core';
import { useForm } from '@mantine/form';
import { closeAllModals, closeModal } from '@mantine/modals';
import { cleanNotifications } from '@mantine/notifications';
import { IconPassword, IconUsers } from '@tabler/icons';
import { useMutation } from '@tanstack/react-query';
import { AxiosError } from 'axios';
import { useSelector } from 'react-redux';
import { Link, useLocation, useNavigate } from 'react-router-dom';
import { userServices } from '../../../services/userServices';
import { RootState, useAppDispatch } from '../../../store';
import { authActions } from '../../../store/authSlice';
import { LoginData } from '../../../types/apis/userLogin';
import { notification } from '../../../utils/notification';

const LoginForm = () => {
  const isLoginModalShow = useSelector(
    (state: RootState) => state.ui.isLoginModalShow
  );
  const navigate = useNavigate();
  const location = useLocation();
  const dispatch = useAppDispatch();
  const form = useForm({
    initialValues: {
      username: '',
      password: '',
    },
    validate: {
      username: (value) =>
        value.trim().length === 0 ? 'Tên đăng nhập không được bỏ trống' : null,
      password: (value) =>
        value.trim().length === 0 ? 'Mật khẩu không được bỏ trống' : null,
    },
    validateInputOnBlur: true,
  });
  const login = useMutation(
    (loginData: LoginData) => {
      return userServices.login(loginData);
    },
    {
      onSuccess: (loginResponse) => {
        if (!isLoginModalShow) {
          navigate(location.state ? location.state : '/', { replace: true });
        } else {
          closeModal('requestLogin');
        }
        cleanNotifications();
        notification.success(
          'Đăng nhập thành công',
          `Xin chào ${loginResponse.data.content.hoTen}`
        );
        setTimeout(() => {
          dispatch(authActions.login(loginResponse.data.content));
        }, 0);
      },
      onError: (error: AxiosError<{ content: string }>) => {
        cleanNotifications();
        let errorData = error.response?.data;
        let errorMessage = errorData?.content;
        if (errorMessage) {
          notification.error('Đăng nhập không thành công', errorMessage);
        }
      },
    }
  );

  if (login.isLoading) {
    notification.loading(
      'Đang thực hiện đăng nhập',
      'Vui lòng chờ trong giây lát'
    );
  }

  function loginSubmitHandler(loginData: {
    username: string;
    password: string;
  }) {
    login.mutate({ taiKhoan: loginData.username, matKhau: loginData.password });
  }

  function cancelClickHandler() {
    if (isLoginModalShow) {
      closeAllModals();
    } else {
      navigate(location.state ? location.state : '/');
    }
  }

  return (
    <form onSubmit={form.onSubmit((values) => loginSubmitHandler(values))}>
      <Stack spacing={'md'}>
        <TextInput
          icon={<IconUsers />}
          placeholder='Tên Đăng Nhập'
          radius='md'
          size='lg'
          {...form.getInputProps('username')}
        />
        <PasswordInput
          icon={<IconPassword />}
          placeholder='Mật Khẩu'
          radius={'md'}
          size='lg'
          autoComplete='on'
          {...form.getInputProps('password')}
        />
        <Text
          component={Link}
          to='/register'
          state={location.state ? location.state : location.pathname}
          onClick={() => closeAllModals()}
          sx={(theme) => ({
            color: theme.colors.crusta[0],

            '&:hover, &:focus': {
              textDecoration: 'underline',
              textUnderlineOffset: '5px',
            },
          })}
        >
          Chưa có tài khoản? Đăng ký ngay để đặt vé
        </Text>
        <Stack>
          <Button type='submit' variant='filled' size={'lg'}>
            Đăng Nhập
          </Button>
          <Button
            onClick={cancelClickHandler}
            type='button'
            variant='outline'
            size='lg'
          >
            Hủy Bỏ
          </Button>
        </Stack>
      </Stack>
    </form>
  );
};

export default LoginForm;
