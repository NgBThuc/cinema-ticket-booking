import { Select, Stack, Title } from '@mantine/core';
import { usePlayingMovies } from '../../hooks/useMovieList';
import Loading from '../ui/Loading';

interface MovieSelectStepProps {
  selectMovieHandler: (value: string) => void;
  value: string;
}

const MovieSelectStep = (props: MovieSelectStepProps) => {
  const playingMoviesQuery = usePlayingMovies();

  if (playingMoviesQuery.isLoading) {
    return <Loading />;
  }

  const selectMovieHandler = (value: string) => {
    props.selectMovieHandler(value);
  };

  return (
    <Stack>
      <Title order={2} color='crusta' align='center'>
        Chọn phim bạn muốn xem
      </Title>
      <Select
        value={props.value}
        data={playingMoviesQuery.data ? playingMoviesQuery.data : []}
        onChange={selectMovieHandler}
        placeholder='Chọn phim'
        clearable
      />
    </Stack>
  );
};

export default MovieSelectStep;
