import { Select, Stack, Title } from '@mantine/core';

interface ScheduleSelectStepProps {
  scheduleSelectData: Array<{
    value: string;
    label: string;
  }>;
  selectScheduleHandler: (value: string) => void;
  value: string;
}

const ScheduleSelectStep = (props: ScheduleSelectStepProps) => {
  const selectScheduleHandler = (value: string) => {
    props.selectScheduleHandler(value);
  };

  return (
    <Stack>
      <Title order={2} color='crusta' align='center'>
        Chọn giờ chiếu phim bạn thích nhất
      </Title>
      <Select
        value={props.value}
        onChange={selectScheduleHandler}
        data={props.scheduleSelectData}
        placeholder='Chọn giờ chiếu phim'
      />
    </Stack>
  );
};

export default ScheduleSelectStep;
