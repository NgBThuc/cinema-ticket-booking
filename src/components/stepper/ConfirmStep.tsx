import { Button, createStyles, Stack, Text, Title } from '@mantine/core';
import { Link } from 'react-router-dom';

const useStyles = createStyles((theme) => ({
  infoWrapperStack: {
    padding: theme.spacing.md,
    backgroundColor: theme.colors.mirage[5],
    borderRadius: theme.radius.md,
    textAlign: 'center',
  },

  infoStack: {
    gap: theme.spacing.xs,
  },

  infoTitle: {
    fontSize: theme.fontSizes.lg,
    fontWeight: 'bold',
    color: theme.colors.crusta[2],
  },

  confirmButton: {
    alignSelf: 'center',
  },
}));

interface BookingData {
  movie: string;
  theater: string;
  bookingId: string;
  time: string;
  date: string;
}

interface ConfirmStepProps {
  bookingData: BookingData;
}

const ConfirmStep = (props: ConfirmStepProps) => {
  const { classes } = useStyles();

  return (
    <Stack>
      <Title order={2} color={'crusta'} align={'center'}>
        Xác nhận thông tin và đặt vé
      </Title>
      <Stack className={classes.infoWrapperStack}>
        <Stack className={classes.infoStack}>
          <Text className={classes.infoTitle}>Phim đã chọn</Text>
          <Text>{props.bookingData.movie}</Text>
        </Stack>
        <Stack className={classes.infoStack}>
          <Text className={classes.infoTitle}>Rạp đã chọn</Text>
          <Text>{props.bookingData.theater}</Text>
        </Stack>
        <Stack className={classes.infoStack}>
          <Text className={classes.infoTitle}>Giờ chiếu đã chọn</Text>
          <Text>
            {props.bookingData.date} - {props.bookingData.time}
          </Text>
        </Stack>
        <Button
          component={Link}
          to={`/booking/${props.bookingData.bookingId}`}
          className={classes.confirmButton}
        >
          Tiến hành đặt vé
        </Button>
      </Stack>
    </Stack>
  );
};

export default ConfirmStep;
