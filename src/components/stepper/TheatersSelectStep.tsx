import { Select, Stack, Title } from '@mantine/core';

interface TheatersSelectStepProps {
  theatersSelectData: {
    value: string;
    label: string;
  }[];
  selectTheaterHandler: (value: string) => void;
  value: string;
}

const TheatersSelectStep = (props: TheatersSelectStepProps) => {
  const selectTheaterHandler = (value: string) => {
    props.selectTheaterHandler(value);
  };

  return (
    <Stack>
      <Title order={2} color='crusta' align='center'>
        Chọn rạp chiếu phim bạn thích nhất
      </Title>
      <Select
        clearable
        value={props.value}
        data={props.theatersSelectData}
        placeholder='Chọn rạp chiếu phim'
        onChange={selectTheaterHandler}
      />
    </Stack>
  );
};

export default TheatersSelectStep;
