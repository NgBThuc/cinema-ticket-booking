import {
  Box,
  Button,
  createStyles,
  Group,
  Stepper,
  Title,
} from '@mantine/core';
import { useMediaQuery } from '@mantine/hooks';
import { useState } from 'react';
import { useBookingInformation } from '../../hooks/useBookingList';
import {
  useSelectSchedule,
  useSelectTheater,
} from '../../hooks/useMovieSchedule';
import { notification } from '../../utils/notification';
import ConfirmStep from './ConfirmStep';
import MovieSelectStep from './MovieSelectStep';
import ScheduleSelectStep from './ScheduleSelectStep';
import TheatersSelectStep from './TheatersSelectStep';

const useStyles = createStyles((theme) => ({
  section: {
    paddingBlock: '5rem',
    backgroundColor: theme.colors.mirage[2],
  },

  stepperRoot: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    gap: theme.spacing.lg,
  },

  stepperContent: {
    width: '100%',
    maxWidth: 600,
    marginInline: 'auto',
    marginBlock: theme.spacing.lg,
  },

  stepperSteps: {
    gap: theme.spacing.md,
    width: '100%',
    [theme.fn.largerThan('md')]: {
      gap: '0px',
    },
  },

  stepperStep: {
    flexDirection: 'column',
    alignItems: 'center',
    width: '100%',
    gap: theme.spacing.md,

    [theme.fn.largerThan('md')]: {
      width: 'auto',
    },
  },

  stepperVerticalSeperator: {
    display: 'none',
  },

  stepperStepBody: {
    margin: 0,
  },

  stepperStepLabel: {
    textAlign: 'center',
  },

  stepperStepDescription: {
    textAlign: 'center',
  },
}));

const BookingStepper = () => {
  const isLargerThanMd = useMediaQuery('(min-width: 992px)');
  const { classes } = useStyles();
  const [activeStep, setActiveStep] = useState(0);
  const [movieId, setMovieId] = useState('');
  const [theaterId, setTheaterId] = useState('');
  const [bookingId, setBookingId] = useState('');
  const theaterSelectQuery = useSelectTheater(movieId);
  const scheduleSelectQuery = useSelectSchedule(movieId, theaterId);
  const bookingInformationQuery = useBookingInformation(bookingId);

  if (theaterSelectQuery.isError) {
    notification.error(
      'Tải thông tin rạp chiếu phim không thành công',
      'Vui lòng thử lại'
    );
  } else if (bookingInformationQuery.isError) {
    notification.error(
      'Tải thông tin đặt vé không thành công',
      'Vui lòng thử lại'
    );
  }

  function nextStep() {
    setActiveStep((currentStep) =>
      currentStep < 4 ? currentStep + 1 : currentStep
    );
  }

  function prevStep() {
    setActiveStep((currentStep) =>
      currentStep > 0 ? currentStep - 1 : currentStep
    );
  }

  function selectMovieHandler(value: string) {
    setMovieId(value);
    if (movieId) {
      theaterSelectQuery.refetch();
    }
  }

  function selectTheaterHandler(value: string) {
    setTheaterId(value);
    if (theaterId) {
      scheduleSelectQuery.refetch();
    }
  }

  function selectScheduleHandler(value: string) {
    setBookingId(value);
    if (bookingId) {
      bookingInformationQuery.refetch();
    }
  }

  function isNextButtonDisable(): boolean {
    if (
      activeStep === 0 &&
      (!movieId || theaterSelectQuery.isLoading || theaterSelectQuery.isError)
    ) {
      return true;
    }
    if (activeStep === 1 && (!theaterId || scheduleSelectQuery.isLoading)) {
      return true;
    }
    if (
      activeStep === 2 &&
      (!bookingId ||
        bookingInformationQuery.isLoading ||
        bookingInformationQuery.isError)
    ) {
      return true;
    }
    if (activeStep === 3) {
      return true;
    }
    return false;
  }

  function isPrevButtonDisable(): boolean {
    if (activeStep === 0) {
      return true;
    }
    return false;
  }

  return (
    <section className={classes.section} id='bookingStepper'>
      <Box
        className={'container'}
        sx={(theme) => ({
          display: 'flex',
          flexDirection: 'column',
          gap: '2rem',
          width: '100%',
        })}
      >
        <Title order={2} color='crusta' align='center' transform='uppercase'>
          Đặt vé nhanh chóng chỉ với 4 bước đơn giản
        </Title>
        <Stepper
          orientation={isLargerThanMd ? 'horizontal' : 'vertical'}
          active={activeStep}
          onStepClick={setActiveStep}
          classNames={{
            root: classes.stepperRoot,
            content: classes.stepperContent,
            steps: classes.stepperSteps,
            step: classes.stepperStep,
            verticalSeparator: classes.stepperVerticalSeperator,
            stepBody: classes.stepperStepBody,
            stepLabel: classes.stepperStepLabel,
            stepDescription: classes.stepperStepDescription,
          }}
        >
          <Stepper.Step
            label='Bước 1'
            description='Chọn phim bạn muốn xem'
            allowStepSelect={false}
          >
            <MovieSelectStep
              value={movieId}
              selectMovieHandler={selectMovieHandler}
            />
          </Stepper.Step>

          <Stepper.Step
            label='Bước 2'
            description='Chọn rạp chiếu phim bạn thích nhất'
            loading={theaterSelectQuery.isFetching}
            allowStepSelect={false}
          >
            <TheatersSelectStep
              value={theaterId}
              selectTheaterHandler={selectTheaterHandler}
              theatersSelectData={
                theaterSelectQuery.data ? theaterSelectQuery.data : []
              }
            />
          </Stepper.Step>

          <Stepper.Step
            label='Bước 3'
            description='Chọn giờ chiếu hợp lý'
            loading={scheduleSelectQuery.isFetching}
            allowStepSelect={false}
          >
            <ScheduleSelectStep
              value={bookingId}
              selectScheduleHandler={selectScheduleHandler}
              scheduleSelectData={
                scheduleSelectQuery.data ? scheduleSelectQuery.data : []
              }
            />
          </Stepper.Step>

          <Stepper.Step
            label='Bước 4'
            description='Xác nhận và đặt vé'
            loading={bookingInformationQuery.isFetching}
            allowStepSelect={false}
          >
            <ConfirmStep
              bookingData={
                bookingInformationQuery.data
                  ? bookingInformationQuery.data
                  : {
                      movie: '',
                      theater: '',
                      bookingId: '',
                      time: '',
                      date: '',
                    }
              }
            />
          </Stepper.Step>
        </Stepper>

        <Group position='center' mt='xl'>
          <Button
            disabled={isPrevButtonDisable()}
            onClick={prevStep}
            variant='filled'
            color='crusta'
          >
            Quay lại
          </Button>
          <Button
            disabled={isNextButtonDisable()}
            onClick={nextStep}
            variant='filled'
            color='crusta'
          >
            Tiếp theo
          </Button>
        </Group>
      </Box>
    </section>
  );
};

export default BookingStepper;
