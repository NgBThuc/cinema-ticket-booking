import {
  Button,
  Center,
  createStyles,
  Stack,
  Text,
  Title,
} from '@mantine/core';
import { Link } from 'react-router-dom';

const useStyles = createStyles((theme) => ({
  root: {
    width: '100vw',
    height: '100vh',
    paddingInline: theme.spacing.md,
  },

  stack: {
    gap: theme.spacing.lg,
    alignItems: 'center',
    width: '100%',
    maxWidth: 600,
    marginBottom: 100,
  },

  label: {
    fontSize: 100,
    fontWeight: 'bold',
    color: theme.colors.lynch[0],
    opacity: '30%',

    [theme.fn.largerThan('md')]: {
      fontSize: 150,
    },
  },

  title: {
    textAlign: 'center',
    fontSize: 25,
  },
}));

const Error404 = () => {
  const { classes } = useStyles();

  return (
    <Center className={classes.root}>
      <Stack className={classes.stack}>
        <div className={classes.label}>404</div>
        <Title
          className={classes.title}
          order={1}
          color='crusta'
          transform='uppercase'
        >
          Có vẻ như bạn đã tìm ra một khu vực bí ẩn
        </Title>
        <Text align='center' color='lynch.0'>
          Tuy nhiên không có gì ở đây cho bạn cả. Có lẽ là bạn đã nhập nhầm địa
          chỉ hoặc là trang này đã được chuyển sang một URL khác.
        </Text>
        <Button component={Link} to='/' variant='filled'>
          Trở về trang chủ
        </Button>
      </Stack>
    </Center>
  );
};

export default Error404;
