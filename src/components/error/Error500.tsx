import {
  Button,
  Center,
  createStyles,
  Stack,
  Text,
  Title,
} from '@mantine/core';
import { Link } from 'react-router-dom';

const useStyles = createStyles((theme) => ({
  root: {
    width: '100vw',
    height: '100vh',
    paddingInline: theme.spacing.md,
  },

  stack: {
    gap: theme.spacing.lg,
    alignItems: 'center',
    width: '100%',
    maxWidth: 600,
    marginBottom: 100,
  },

  label: {
    fontSize: 100,
    fontWeight: 'bold',
    color: theme.colors.lynch[0],
    opacity: '30%',

    [theme.fn.largerThan('md')]: {
      fontSize: 150,
    },
  },

  title: {
    textAlign: 'center',
    fontSize: 25,
  },
}));

const Error500 = () => {
  const { classes } = useStyles();

  const reloadHandler = () => {
    window.history.back();
  };

  return (
    <Center className={classes.root}>
      <Stack className={classes.stack}>
        <div className={classes.label}>500</div>
        <Title
          className={classes.title}
          order={1}
          color='crusta'
          transform='uppercase'
        >
          Có vẻ như vừa có điều gì đó tồi tệ xảy ra ...
        </Title>
        <Text align='center' color='lynch.0'>
          Server của chúng tôi không thể xử lý yêu cầu của bạn. Đừng lo lắng,
          chúng tôi đã được thông báo về vấn đề này và sẽ sửa chữa trong thời
          gian sớm nhất.
        </Text>
        <Text align='center' color='crusta.0' weight='bold'>
          Vui lòng tải lại trang sau vài phút.
        </Text>
        <Button
          onClick={reloadHandler}
          component={Link}
          to='/'
          variant='filled'
        >
          Tải lại trang
        </Button>
      </Stack>
    </Center>
  );
};

export default Error500;
