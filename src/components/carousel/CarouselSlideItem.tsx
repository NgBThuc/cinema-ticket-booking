import { Carousel } from '@mantine/carousel';
import { Image } from '@mantine/core';
import { BannerList } from '../../types/apis/bannerList';

interface CarouselSlideItemProps {
  bannerList: BannerList;
}

const CarouselSlideItem = (props: CarouselSlideItemProps) => {
  const carouselContent = props.bannerList.map((item) => {
    return (
      <Carousel.Slide key={item.maPhim}>
        <Image src={item.hinhAnh} height={'100%'} />
      </Carousel.Slide>
    );
  });

  return <>{carouselContent}</>;
};

export default CarouselSlideItem;
