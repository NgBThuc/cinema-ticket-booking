import { Carousel } from '@mantine/carousel';
import { createStyles, Group, Stack } from '@mantine/core';
import { FC } from 'react';
import { useBannerList } from '../../hooks/useBannerList';
import Loading from '../ui/Loading';
import CarouselSlideItem from './CarouselSlideItem';
import CarouselTitle from './CarouselTitle';

const useStyles = createStyles((theme, _params, getRef) => ({
  group: {
    height: 600,

    [theme.fn.smallerThan('md')]: {
      flexDirection: 'column',
    },

    [theme.fn.smallerThan('sm')]: {
      height: 450,
    },
  },

  titleWrapper: {
    width: '35%',
    fontStyle: 'italic',

    [theme.fn.smallerThan('md')]: {
      width: '100%',
      textAlign: 'center',
      fontStyle: 'normal',
    },
  },

  carouselRoot: {
    width: '75%',

    [theme.fn.smallerThan('md')]: {
      width: '100%',
    },

    '&:hover': {
      [`& .${getRef('controls')}`]: {
        opacity: 1,
      },
    },
  },

  carouselControls: {
    ref: getRef('controls'),
    transition: 'opacity 150ms ease',
    opacity: 0,

    [theme.fn.smallerThan('sm')]: {
      display: 'none',
    },
  },
}));

const HomePageCarousel: FC = () => {
  const { classes } = useStyles();
  const bannerListQuery = useBannerList();

  if (bannerListQuery.isLoading) {
    return <Loading />;
  }

  return (
    <Group
      className={`container ${classes.group}`}
      position='center'
      noWrap={true}
      spacing='xl'
      my='xl'
    >
      <Stack className={classes.titleWrapper}>
        <CarouselTitle />
      </Stack>
      <Carousel
        classNames={{
          root: classes.carouselRoot,
          controls: classes.carouselControls,
        }}
        withIndicators
        controlSize={50}
        loop={true}
      >
        <CarouselSlideItem
          bannerList={bannerListQuery.data ? bannerListQuery.data : []}
        />
      </Carousel>
    </Group>
  );
};

export default HomePageCarousel;
