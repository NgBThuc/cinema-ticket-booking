import { createStyles, Text, Title } from '@mantine/core';
import { useMediaQuery } from '@mantine/hooks';
import { FC } from 'react';

const useStyles = createStyles((theme) => ({
  title: {
    fontSize: '50px',

    [theme.fn.smallerThan('sm')]: {
      fontSize: 30,
    },
  },

  subtitle: {
    fontSize: '25px',
  },

  brandName: {
    fontWeight: 'bold',
    color: theme.colors.crusta[3],
  },
}));

const CarouselTitle: FC = () => {
  const { classes } = useStyles();
  const isDesktop = useMediaQuery('(min-width: 992px)');

  return (
    <>
      <Title color={'crusta'} className={classes.title} order={1}>
        Phim Hot {isDesktop && <br />} Trong Tháng
      </Title>
      <Text className={classes.subtitle}>
        Đặt vé xem phim tiện lợi và nhanh chóng cùng{' '}
        <span className={classes.brandName}>CineTick</span>
      </Text>
    </>
  );
};

export default CarouselTitle;
