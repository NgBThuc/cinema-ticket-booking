import { Carousel } from '@mantine/carousel';
import { Box, createStyles, Grid, Stack } from '@mantine/core';
import { useMoviesList } from '../../hooks/useMovieList';
import Loading from '../ui/Loading';
import MovieCard from './MovieCard';
import MoviesListHeader from './MoviesListHeader';

const useStyles = createStyles((theme) => ({
  wrapper: {
    paddingBlock: theme.spacing.xl,
  },

  gridCol: {
    [theme.fn.smallerThan('md')]: {
      flexBasis: '50%',
      maxWidth: '50%',
    },

    [theme.fn.smallerThan('sm')]: {
      flexBasis: '100%',
      maxWidth: '100%',
    },
  },

  titleWrapper: {
    textAlign: 'center',
  },
}));

const MoviesList = () => {
  const { classes } = useStyles();
  const moviesListQuery = useMoviesList();

  if (moviesListQuery.isLoading) {
    return <Loading />;
  }

  const moviesListContent = moviesListQuery.data?.map((moviesSlide, index) => {
    return (
      <Carousel.Slide key={index}>
        <Grid columns={12}>
          {moviesSlide.map((movie) => (
            <Grid.Col className={classes.gridCol} key={movie.maPhim} span={4}>
              <MovieCard
                id={movie.maPhim}
                image={movie.hinhAnh}
                title={movie.tenPhim}
                description={movie.moTa}
                rating={movie.danhGia}
              />
            </Grid.Col>
          ))}
        </Grid>
      </Carousel.Slide>
    );
  });

  return (
    <Box component='section' className={classes.wrapper} id='moviesList'>
      <Stack my={'xl'} spacing='lg' className='container'>
        <Stack className={classes.titleWrapper} spacing='xs'>
          <MoviesListHeader />
        </Stack>
        <Carousel
          align={'center'}
          withControls={false}
          withIndicators={true}
          slideGap='lg'
          loop={true}
          pb={50}
        >
          {moviesListContent}
        </Carousel>
      </Stack>
    </Box>
  );
};

export default MoviesList;
