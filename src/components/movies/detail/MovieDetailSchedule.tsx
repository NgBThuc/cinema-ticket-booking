import {
  Accordion,
  Avatar,
  Divider,
  Grid,
  Group,
  Spoiler,
  Stack,
  Text,
  Title,
} from '@mantine/core';
import { useMediaQuery } from '@mantine/hooks';
import { useParams } from 'react-router-dom';
import { useMovieSchedule } from '../../../hooks/useMovieSchedule';
import {
  TheaterBranch,
  TheaterSystem,
} from '../../../types/apis/movieSchedule';
import Loading from '../../ui/Loading';
import MovieScheduleBadge from './MovieScheduleBadge';

const MovieDetailSchedule = () => {
  const params = useParams<{ movieId: string }>();
  const movieScheduleQuery = useMovieSchedule(params.movieId!);
  const isLargerThanMd = useMediaQuery('(min-width: 992px)');

  if (movieScheduleQuery.isLoading) {
    return <Loading />;
  }

  function movieScheduleDetailContent(theaterBranch: TheaterBranch) {
    return theaterBranch.lichChieuPhim.map((movieSchedule) => {
      return (
        <Grid.Col key={movieSchedule.maLichChieu} span={isLargerThanMd ? 3 : 6}>
          <MovieScheduleBadge
            scheduleId={movieSchedule.maLichChieu}
            time={movieSchedule.ngayChieuGioChieu}
          />
        </Grid.Col>
      );
    });
  }

  function theaterBranchScheduleContent(theaterGroup: TheaterSystem) {
    return theaterGroup.cumRapChieu.map((theaterBranch) => {
      return (
        <Stack key={theaterBranch.maCumRap} py='md'>
          <Stack spacing='sm'>
            <Text
              size='md'
              weight='bold'
              sx={(theme) => ({ color: theme.colors.crusta[1] })}
            >
              {theaterBranch.tenCumRap}
            </Text>
            <Text italic={true} size='md'>
              {theaterBranch.diaChi}
            </Text>
          </Stack>
          <Spoiler
            maxHeight={125}
            showLabel='Hiện thêm lịch chiếu'
            hideLabel='Thu hẹp'
            styles={(theme) => ({
              control: {
                marginTop: theme.spacing.md,
                color: theme.colors.lynch[1],

                '&:hover': {
                  color: theme.colors.crusta[3],
                },
              },
            })}
          >
            <Grid columns={12}>
              {movieScheduleDetailContent(theaterBranch)}
            </Grid>
          </Spoiler>
        </Stack>
      );
    });
  }

  const movieScheduleContent = movieScheduleQuery.data?.heThongRapChieu.map(
    (theaterGroup) => {
      return (
        <Accordion
          key={theaterGroup.maHeThongRap}
          styles={(theme) => ({
            content: {
              '& > * + *': {
                borderTop: `2px solid ${theme.colors.crusta[0]}`,
              },
            },
          })}
        >
          <Accordion.Item value={theaterGroup.maHeThongRap}>
            <Accordion.Control
              sx={(theme) => ({
                '&:hover': {
                  backgroundColor: theme.colors.mirage[0],
                  borderTopLeftRadius: theme.radius.md,
                  borderTopRightRadius: theme.radius.md,
                },
              })}
            >
              <Group>
                <Avatar src={theaterGroup.logo} />
                <Title
                  order={3}
                  size={20}
                  sx={(theme) => ({ color: theme.colors.lynch[0] })}
                  transform='uppercase'
                >
                  {theaterGroup.tenHeThongRap}
                </Title>
              </Group>
            </Accordion.Control>
            <Accordion.Panel>
              {theaterBranchScheduleContent(theaterGroup)}
            </Accordion.Panel>
          </Accordion.Item>
        </Accordion>
      );
    }
  );

  return (
    <>
      <Divider
        sx={(theme) => ({
          [theme.fn.largerThan('md')]: {
            display: 'none',
          },
        })}
      />
      <Stack>
        <Title
          sx={(theme) => ({
            [theme.fn.largerThan('md')]: {
              fontSize: '2rem',
            },
          })}
          variant='gradient'
          order={2}
          color='crusta'
          weight='bold'
        >
          Lịch chiếu
        </Title>
        <Stack>{movieScheduleContent}</Stack>
      </Stack>
    </>
  );
};

export default MovieDetailSchedule;
