import {
  Badge,
  createStyles,
  Divider,
  Group,
  Stack,
  Text,
  Title,
} from '@mantine/core';
import dayjs from 'dayjs';
import { useParams } from 'react-router-dom';
import { useMovieInformation } from '../../../hooks/useMovieInformation';
import Loading from '../../ui/Loading';

const useStyles = createStyles((theme) => ({
  root: {
    width: '100%',

    [theme.fn.largerThan('md')]: {
      width: '60%',
    },
  },

  badge: {
    alignSelf: 'start',
    color: '#ffffff',
  },

  hotBadge: {
    backgroundColor: 'red',
    color: '#ffffff',
  },
}));

const MovieDetailInformation = () => {
  const params = useParams<{ movieId: string }>();
  const { classes } = useStyles();
  const movieInformationQuery = useMovieInformation(params.movieId!);

  if (movieInformationQuery.isLoading) {
    return <Loading />;
  }

  return (
    <Stack className={classes.root}>
      <Stack>
        <Title variant='gradient' color={'crusta'} order={1}>
          {movieInformationQuery.data?.tenPhim}
        </Title>
        <Stack>
          <Group>
            <Badge
              variant='gradient'
              color={'crusta'}
              size={'lg'}
              leftSection={'Rating: '}
              gradient={{ from: '#2e56f5', to: '#00bdd6' }}
            >
              {movieInformationQuery.data?.danhGia}/10
            </Badge>
            {movieInformationQuery.data?.sapChieu && (
              <Badge
                variant='gradient'
                className={classes.badge}
                size='lg'
                gradient={{ from: '#2e56f5', to: '#00bdd6' }}
              >
                SẮP CHIẾU
              </Badge>
            )}
          </Group>
          <Group>
            <Badge
              variant='gradient'
              className={classes.badge}
              size='lg'
              leftSection={'Ngày khởi chiếu: '}
            >
              {dayjs(movieInformationQuery.data?.ngayKhoiChieu).format(
                'DD/MM/YYYY'
              )}
            </Badge>
            {movieInformationQuery.data?.hot && (
              <Badge variant='gradient' className={classes.hotBadge} size='lg'>
                HOT MOVIE THIS MONTH
              </Badge>
            )}
          </Group>
        </Stack>
      </Stack>
      <Divider></Divider>
      <Stack>
        <Title variant='gradient' order={2} color='crusta' weight={'bold'}>
          Mô tả
        </Title>
        <Text>{movieInformationQuery.data?.moTa}</Text>
      </Stack>
    </Stack>
  );
};

export default MovieDetailInformation;
