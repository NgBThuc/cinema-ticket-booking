import { Badge, Group } from '@mantine/core';
import dayjs from 'dayjs';
import { Link } from 'react-router-dom';

interface MovieScheduleBadgeProps {
  time: string;
  scheduleId: string;
}

const MovieScheduleBadge = (props: MovieScheduleBadgeProps) => {
  return (
    <Badge
      component={Link}
      to={`/booking/${props.scheduleId}`}
      size='lg'
      sx={(theme) => ({
        backgroundColor: theme.colors.crusta[6],
        color: '#ffffff',
        transition: 'all 300ms ease',
        cursor: 'pointer',

        '&:hover': {
          backgroundColor: theme.colors.crusta[3],
        },
      })}
    >
      <Group spacing={'xs'} noWrap={true}>
        <span>{dayjs(props.time).format('DD/MM/YYYY')}</span>
        <span>{dayjs(props.time).format('HH:mm')}</span>
      </Group>
    </Badge>
  );
};

export default MovieScheduleBadge;
