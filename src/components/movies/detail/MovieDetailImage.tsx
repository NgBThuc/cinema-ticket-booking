import {
  ActionIcon,
  BackgroundImage,
  Center,
  createStyles,
} from '@mantine/core';
import { IconPlayerPlay } from '@tabler/icons';
import { useParams } from 'react-router-dom';
import { useMovieInformation } from '../../../hooks/useMovieInformation';
import { openMovieTrailerModal } from '../../../utils/modal';
import Loading from '../../ui/Loading';

const useStyles = createStyles((theme) => ({
  root: {
    width: '100%',
    height: '500px',

    [theme.fn.largerThan('md')]: {
      width: '40%',
    },
  },

  trailerButtonIcon: {
    borderRadius: '50%',
    opacity: '80%',
  },
}));

const MovieDetailImage = () => {
  const { classes } = useStyles();
  const params = useParams<{ movieId: string }>();
  const movieInformationQuery = useMovieInformation(params.movieId!);

  if (movieInformationQuery.isLoading) {
    return <Loading />;
  }

  const trailerButtonClickHandler = () => {
    if (movieInformationQuery.data) {
      openMovieTrailerModal(
        movieInformationQuery.data?.tenPhim,
        movieInformationQuery.data?.trailer
      );
    }
  };

  return (
    <BackgroundImage
      className={classes.root}
      radius='md'
      src={movieInformationQuery.data ? movieInformationQuery.data.hinhAnh : ''}
      sx={(theme) => ({
        border: `2px solid ${theme.colors.lynch[2]}`,
      })}
    >
      {movieInformationQuery.data && (
        <Center sx={{ width: '100%', height: '100%' }}>
          <ActionIcon
            size='xl'
            className={classes.trailerButtonIcon}
            variant='gradient'
            gradient={{ from: '#2e56f5', to: '#00bdd6' }}
            onClick={trailerButtonClickHandler}
          >
            <IconPlayerPlay size={25} />
          </ActionIcon>
        </Center>
      )}
    </BackgroundImage>
  );
};

export default MovieDetailImage;
