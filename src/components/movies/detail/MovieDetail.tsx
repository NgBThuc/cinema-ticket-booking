import { Box, Stack } from '@mantine/core';
import MovieDetailImage from './MovieDetailImage';
import MovieDetailInformation from './MovieDetailInformation';
import MovieDetailSchedule from './MovieDetailSchedule';

const MovieDetail = () => {
  return (
    <Box sx={(theme) => ({ paddingBlock: theme.spacing.xl })}>
      <Stack className='container'>
        <Stack
          sx={(theme) => ({
            [theme.fn.largerThan('md')]: {
              flexDirection: 'row',
              alignItems: 'center',
              gap: theme.spacing.lg,
              marginBottom: theme.spacing.lg,
            },
          })}
        >
          <MovieDetailImage />
          <MovieDetailInformation />
        </Stack>
        <MovieDetailSchedule />
      </Stack>
    </Box>
  );
};

export default MovieDetail;
