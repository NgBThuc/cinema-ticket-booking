import { createStyles, Text, Title } from '@mantine/core';

const useStyles = createStyles((theme) => ({
  title: {
    fontSize: 40,

    [theme.fn.smallerThan('sm')]: {
      fontSize: 30,
    },
  },

  subtitle: {
    fontSize: '25px',
  },
}));

const MoviesListHeader = () => {
  const { classes } = useStyles();

  return (
    <>
      <Title className={classes.title} order={2} color='crusta'>
        PHIM ĐANG ĐƯỢC CHIẾU TẠI RẠP
      </Title>
      <Text className={classes.subtitle}>Đặt vé ngay</Text>
    </>
  );
};

export default MoviesListHeader;
