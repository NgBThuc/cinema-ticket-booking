import {
  Badge,
  Button,
  Card,
  createStyles,
  Group,
  Image,
  Text,
} from '@mantine/core';
import { FC } from 'react';
import { Link } from 'react-router-dom';

interface MovieCardProps {
  id: number;
  image: string;
  title: string;
  description: string;
  rating: number;
}

const useStyles = createStyles((theme) => ({
  card: {
    height: 750,
    display: 'flex',
    flexDirection: 'column',
  },

  badge: {
    overflow: 'visible',
  },
}));

const MovieCard: FC<MovieCardProps> = (props) => {
  const { classes } = useStyles();

  return (
    <Card className={classes.card} shadow='sm' p='lg' radius='md' withBorder>
      <Card.Section>
        <Image
          src={props.image}
          alt={props.title}
          fit='cover'
          height={450}
          radius={'md'}
        />
      </Card.Section>

      <Group noWrap={true} position='apart' mt='md' mb='xs'>
        <Text size={'xl'} color='crusta' lineClamp={1} weight={700}>
          {props.title}
        </Text>
        <Badge
          className={classes.badge}
          color='crusta'
          variant='filled'
          size='lg'
        >
          {props.rating}/10
        </Badge>
      </Group>

      <Text lineClamp={4} size='lg' color='lynch' mt={'sm'}>
        {props.description}
      </Text>

      <Button
        component={Link}
        to={`/movies/${props.id}`}
        variant='outline'
        fullWidth
        mt='auto'
        radius='md'
      >
        Xem chi tiết
      </Button>
    </Card>
  );
};

export default MovieCard;
