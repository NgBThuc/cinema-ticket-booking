import {
  Anchor,
  Button,
  CloseButton,
  clsx,
  createStyles,
  Group,
  Navbar,
  Stack,
  Title,
} from '@mantine/core';
import { useMediaQuery } from '@mantine/hooks';
import { IconHome, IconLogout, IconMovie, IconUsers } from '@tabler/icons';
import { useSelector } from 'react-redux';
import { Link, useLocation, useNavigate } from 'react-router-dom';
import { RootState, useAppDispatch } from '../../store';
import { authActions } from '../../store/authSlice';
import { uiActions } from '../../store/uiSlice';
import BrandLogo from '../ui/BrandLogo';

const useStyles = createStyles((theme) => ({
  root: {
    position: 'fixed',
    top: 0,
    left: '-100%',
    width: '100%',
    background: theme.colors.mirage[6],
    transition: 'left 300ms ease',
    height: '100%',
    flexShrink: 0,

    [theme.fn.largerThan('xs')]: {
      width: '50%',
    },

    [theme.fn.largerThan('sm')]: {
      width: '40%',
    },

    [theme.fn.largerThan('md')]: {
      position: 'relative',
      left: 0,
      width: 300,
      height: 'calc(100vh - 70px)',
      minHeight: 800,
    },
  },

  navBarShow: {
    left: 0,
  },

  closeButton: {
    marginLeft: 'auto',
  },

  links: {
    gap: theme.spacing.lg,
    marginTop: theme.spacing.xl,

    [theme.fn.largerThan('md')]: {
      marginTop: theme.spacing.xs,
    },
  },

  link: {
    display: 'flex',
    alignItems: 'center',
    gap: theme.spacing.md,
    paddingInline: theme.spacing.lg,
    paddingBlock: theme.spacing.sm,
    borderRadius: theme.radius.md,
    background: theme.colors.lynch[9],
    color: theme.white,

    '&:hover, &:focus': {
      background: theme.colors.lynch[7],
    },
  },

  linkActive: {
    background: theme.colors.crusta[3],

    '&:hover, &:focus': {
      color: theme.white,
      background: theme.colors.crusta[3],
    },
  },
}));

const AdminNavBar = () => {
  const { classes } = useStyles();
  const location = useLocation();
  const dispatch = useAppDispatch();
  const navigate = useNavigate();
  const isSmallerThanMd = useMediaQuery('(max-width: 992px)');
  const isAdminNavBarShow = useSelector(
    (state: RootState) => state.ui.isAdminNavBarShow
  );

  const linksData = [
    {
      link: '/admin/users',
      label: 'Người Dùng',
      icon: IconUsers,
    },
    {
      link: '/admin/movies',
      label: 'Phim',
      icon: IconMovie,
    },
  ];

  const linksContent = linksData.map((linkItem) => {
    return (
      <Anchor
        className={clsx(
          classes.link,
          location.pathname.includes(linkItem.link) && classes.linkActive
        )}
        component={Link}
        to={linkItem.link}
        key={linkItem.label}
        onClick={() => {
          dispatch(uiActions.toggleAdminNavBar());
        }}
      >
        <linkItem.icon />
        <span>{linkItem.label}</span>
      </Anchor>
    );
  });

  const logoutButtonClickHandler = () => {
    dispatch(authActions.logout());
    navigate('/', { replace: true });
  };

  const closeButtonClickHandler = () => {
    dispatch(uiActions.toggleAdminNavBar());
  };

  return (
    <Navbar
      className={clsx(classes.root, isAdminNavBarShow && classes.navBarShow)}
      p='md'
    >
      <Navbar.Section grow>
        {isSmallerThanMd && (
          <Group align={'center'}>
            <BrandLogo />
            <Title order={1} size={20} weight='bold' variant='gradient'>
              CineTick Admin
            </Title>
            <CloseButton
              className={classes.closeButton}
              onClick={closeButtonClickHandler}
              size='xl'
            />
          </Group>
        )}
        <Stack className={classes.links}>{linksContent}</Stack>
      </Navbar.Section>
      <Navbar.Section>
        <Stack>
          <Button
            component={Link}
            to='/'
            leftIcon={<IconHome />}
            variant='filled'
            color='crusta'
          >
            Homepage
          </Button>
          <Button
            leftIcon={<IconLogout />}
            variant='outline'
            color='crusta'
            onClick={logoutButtonClickHandler}
          >
            Logout
          </Button>
        </Stack>
      </Navbar.Section>
    </Navbar>
  );
};

export default AdminNavBar;
