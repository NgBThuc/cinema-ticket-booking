import { Box, Burger, createStyles } from '@mantine/core';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { RootState, useAppDispatch } from '../../store';
import { uiActions } from '../../store/uiSlice';
import BrandLogo from '../ui/BrandLogo';

const useStyles = createStyles((theme) => ({
  root: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '100vw',
    paddingInline: theme.spacing.md,
    background: theme.colors.mirage[6],
    height: 70,
    borderBottom: `2px solid ${theme.colors.mirage[3]}`,
  },

  burgerButton: {
    [theme.fn.largerThan('md')]: {
      display: 'none',
    },
  },
}));

const AdminHeader = () => {
  const { classes } = useStyles();
  const dispatch = useAppDispatch();
  const isAdminNavBarShow = useSelector(
    (state: RootState) => state.ui.isAdminNavBarShow
  );
  const isAdminNavBarOpened = useSelector(
    (state: RootState) => state.ui.isAdminNavBarShow
  );

  const burgerClickHandler = () => {
    dispatch(uiActions.toggleAdminNavBar());
  };

  return (
    <header className={classes.root}>
      <Box component={Link} to='/'>
        <BrandLogo />
      </Box>
      {!isAdminNavBarShow && (
        <Burger
          className={classes.burgerButton}
          opened={isAdminNavBarOpened}
          onClick={burgerClickHandler}
        />
      )}
    </header>
  );
};

export default AdminHeader;
