import {
  Box,
  Button,
  clsx,
  createStyles,
  Group,
  Image,
  Loader,
  Table,
  Text,
  TextInput,
} from '@mantine/core';
import { cleanNotifications } from '@mantine/notifications';
import {
  IconAdjustments,
  IconCalendar,
  IconSearch,
  IconTrash,
} from '@tabler/icons';
import { useMutation, useQueryClient } from '@tanstack/react-query';
import dayjs from 'dayjs';
import { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { useAdminMoviesList } from '../../../hooks/useMovieList';
import { movieServices } from '../../../services/movieServices';
import { MovieItem } from '../../../types/apis/moviesList';
import { openMovieTrailerModal } from '../../../utils/modal';
import { notification } from '../../../utils/notification';
import { table } from '../../../utils/table';
import AdminTableHeading from '../AdminTableHeading';
import AdminTableNormalHeading from '../AdminTableNormalHeading';

const useStyles = createStyles((theme) => ({
  tableWrapper: {
    marginInline: 'auto',
    width: '100%',
    height: 550,
    minWidth: 0,
    overflow: 'auto',
  },

  table: {
    borderCollapse: 'collapse',
    width: '1800px',
  },
}));

const AdminMoviesTable = () => {
  const { classes } = useStyles();
  const queryClient = useQueryClient();
  const moviesListQuery = useAdminMoviesList();
  const [searchValue, setSearchValue] = useState('');
  const [sortedData, setSortedData] = useState(
    moviesListQuery.data ? moviesListQuery.data : null
  );
  const [sortBy, setSortBy] = useState<keyof MovieItem | null>(null);
  const [isReverseSortDirection, setIsReverseSortDirection] = useState(false);
  const deleteMovie = useMutation(
    (movieId: number) => {
      return movieServices.deleteMovie(movieId);
    },
    {
      onSuccess: () => {
        cleanNotifications();
        notification.success(
          'Xóa phim thành công',
          'Thông tin phim đã được xóa khởi cơ sở dữ liệu'
        );
      },
      onError: () => {
        cleanNotifications();
        notification.error('Xóa phim không thành công', 'Đã có lỗi xảy ra');
      },
    }
  );

  useEffect(() => {
    if (moviesListQuery.data) {
      setSortedData(moviesListQuery.data);
    }
  }, [moviesListQuery.data]);

  if (deleteMovie.isLoading) {
    notification.loading('Thực hiện xóa phim', 'Vui lòng đợi trong giây lát');
  }

  const setSorting = (field: keyof MovieItem) => {
    const reversed = field === sortBy ? !isReverseSortDirection : false;

    setIsReverseSortDirection(reversed);
    setSortBy(field);

    if (moviesListQuery.data) {
      setSortedData(
        table.sortData(moviesListQuery.data, {
          sortBy: field,
          isReversed: reversed,
          search: searchValue,
        })
      );
    }
  };

  const searchChangeHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { value } = event.currentTarget;
    setSearchValue(value);
    if (moviesListQuery.data) {
      setSortedData(
        table.sortData(moviesListQuery.data, {
          sortBy: sortBy,
          isReversed: isReverseSortDirection,
          search: value,
        })
      );
    }
  };

  const deleteButtonClickHandler = (movieId: number) => {
    deleteMovie.mutate(movieId, {
      onSuccess: () => {
        queryClient.invalidateQueries(['moviesList']);
      },
    });
  };

  const trailerButtonClickHandler = (
    movieName: string,
    trailerLink: string
  ) => {
    openMovieTrailerModal(movieName, trailerLink);
  };

  const rowsContent = sortedData?.map((dataItem) => (
    <tr key={dataItem.maPhim}>
      <td width='1%'>{dataItem.maPhim}</td>
      <td width='1%'>
        <Image
          radius='md'
          width={50}
          height={70}
          src={dataItem.hinhAnh}
          fit='contain'
        />
      </td>
      <td width={250}>
        <Text color='crusta' lineClamp={2}>
          {dataItem.tenPhim}
        </Text>
      </td>
      <td width={70}>
        <Text lineClamp={2}>{dataItem.moTa}</Text>
      </td>
      <td width='1%'>
        <Button
          onClick={trailerButtonClickHandler.bind(
            null,
            dataItem.tenPhim,
            dataItem.trailer
          )}
          size='xs'
          variant='outline'
        >
          Xem Trailer
        </Button>
      </td>
      <td width='1%'>{dayjs(dataItem.ngayKhoiChieu).format('DD/MM/YYYY')}</td>
      <td width='1%'>{dataItem.danhGia.toFixed(2)}</td>
      <td width='1%'>{dataItem.hot ? 'Có' : 'Không'}</td>
      <td width='1%'>{dataItem.dangChieu ? 'Có' : 'Không'}</td>
      <td width='1%'>{dataItem.sapChieu ? 'Có' : 'Không'}</td>
      <td width='1%'>
        <Group spacing='xs' noWrap={true}>
          <Button
            size='xs'
            onClick={() => deleteButtonClickHandler(dataItem.maPhim)}
          >
            <IconTrash size={18} />
          </Button>
          <Button
            component={Link}
            to={`/admin/movies/edit/${dataItem.maPhim}`}
            size='xs'
          >
            <IconAdjustments size={18} />
          </Button>
          <Button
            component={Link}
            to={`/admin/movies/schedule/${dataItem.maPhim}`}
            size='xs'
          >
            <IconCalendar size={18} />
          </Button>
        </Group>
      </td>
    </tr>
  ));

  return (
    <>
      {moviesListQuery.isLoading && <Loader sx={{ alignSelf: 'center' }} />}
      {moviesListQuery.data && (
        <>
          <TextInput
            placeholder='Tìm người dùng'
            icon={<IconSearch />}
            value={searchValue}
            onChange={searchChangeHandler}
            radius='md'
          />
          <Box className={clsx(classes.tableWrapper, 'custom-scrollbar')}>
            <Table
              className={classes.table}
              horizontalSpacing='md'
              verticalSpacing='xs'
            >
              <thead>
                <tr>
                  <AdminTableHeading
                    isSorted={sortBy === 'maPhim'}
                    isReversed={isReverseSortDirection}
                    onSort={() => setSorting('maPhim')}
                  >
                    Mã phim
                  </AdminTableHeading>
                  <AdminTableHeading
                    isSorted={sortBy === 'hinhAnh'}
                    isReversed={isReverseSortDirection}
                    onSort={() => setSorting('hinhAnh')}
                    canNotSort={true}
                  >
                    Hình ảnh
                  </AdminTableHeading>
                  <AdminTableHeading
                    isSorted={sortBy === 'tenPhim'}
                    isReversed={isReverseSortDirection}
                    onSort={() => setSorting('tenPhim')}
                  >
                    Tên phim
                  </AdminTableHeading>
                  <AdminTableHeading
                    isSorted={sortBy === 'moTa'}
                    isReversed={isReverseSortDirection}
                    onSort={() => setSorting('moTa')}
                  >
                    Mô tả
                  </AdminTableHeading>
                  <AdminTableHeading
                    isSorted={sortBy === 'trailer'}
                    isReversed={isReverseSortDirection}
                    onSort={() => setSorting('trailer')}
                    canNotSort={true}
                  >
                    Trailer Link
                  </AdminTableHeading>
                  <AdminTableHeading
                    isSorted={sortBy === 'ngayKhoiChieu'}
                    isReversed={isReverseSortDirection}
                    onSort={() => setSorting('ngayKhoiChieu')}
                  >
                    Ngày khởi chiếu
                  </AdminTableHeading>
                  <AdminTableHeading
                    isSorted={sortBy === 'danhGia'}
                    isReversed={isReverseSortDirection}
                    onSort={() => setSorting('danhGia')}
                  >
                    Đánh giá
                  </AdminTableHeading>
                  <AdminTableHeading
                    isSorted={sortBy === 'hot'}
                    isReversed={isReverseSortDirection}
                    onSort={() => setSorting('hot')}
                  >
                    Hot
                  </AdminTableHeading>
                  <AdminTableHeading
                    isSorted={sortBy === 'dangChieu'}
                    isReversed={isReverseSortDirection}
                    onSort={() => setSorting('dangChieu')}
                  >
                    Đang chiếu
                  </AdminTableHeading>
                  <AdminTableHeading
                    isSorted={sortBy === 'sapChieu'}
                    isReversed={isReverseSortDirection}
                    onSort={() => setSorting('sapChieu')}
                  >
                    Sắp chiếu
                  </AdminTableHeading>
                  <AdminTableNormalHeading>Thao tác</AdminTableNormalHeading>
                </tr>
              </thead>
              <tbody>
                {(rowsContent?.length ?? 0) > 0 ? (
                  rowsContent
                ) : (
                  <tr>
                    <td colSpan={11}>
                      <Text
                        weight='bold'
                        size='xl'
                        color='crusta'
                        align='center'
                      >
                        Không có dữ liệu
                      </Text>
                    </td>
                  </tr>
                )}
              </tbody>
            </Table>
          </Box>
        </>
      )}
    </>
  );
};

export default AdminMoviesTable;
