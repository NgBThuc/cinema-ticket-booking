import {
  Box,
  Button,
  clsx,
  createStyles,
  Group,
  Loader,
  Table,
  Text,
  TextInput,
} from '@mantine/core';
import { cleanNotifications } from '@mantine/notifications';
import { IconAdjustments, IconSearch, IconTrash } from '@tabler/icons';
import { useMutation, useQueryClient } from '@tanstack/react-query';
import { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { useUsersList } from '../../../hooks/useUsersList';
import { userServices } from '../../../services/userServices';
import { UsersListItem } from '../../../types/apis/userList';
import { notification } from '../../../utils/notification';
import { table } from '../../../utils/table';
import AdminTableHeading from '../AdminTableHeading';
import AdminTableNormalHeading from '../AdminTableNormalHeading';

const useStyles = createStyles((theme) => ({
  tableWrapper: {
    marginInline: 'auto',
    width: '100%',
    height: 550,
    minWidth: 0,
    overflow: 'auto',
  },

  table: {
    borderCollapse: 'collapse',
  },
}));

const AdminUsersTable = () => {
  const { classes } = useStyles();
  const queryClient = useQueryClient();
  const usersListQuery = useUsersList();
  const [searchValue, setSearchValue] = useState('');
  const [sortedData, setSortedData] = useState(
    usersListQuery.data ? usersListQuery.data : null
  );
  const [sortBy, setSortBy] = useState<keyof UsersListItem | null>(null);
  const [isReverseSortDirection, setIsReverseSortDirection] = useState(false);
  const deleteUser = useMutation(
    (username: string) => {
      return userServices.deleteUser(username);
    },
    {
      onSuccess: () => {
        cleanNotifications();
        notification.success(
          'Xóa người dùng thành công',
          'Thông tin người dùng đã được xóa khởi cơ sở dữ liệu'
        );
      },
      onError: () => {
        cleanNotifications();
        notification.error(
          'Xóa người dùng không thành công',
          'Đã có lỗi xảy ra'
        );
      },
    }
  );

  useEffect(() => {
    if (usersListQuery.data) {
      setSortedData(usersListQuery.data);
    }
  }, [usersListQuery.data]);

  if (deleteUser.isLoading) {
    notification.loading(
      'Thực hiện xóa người dùng',
      'Vui lòng đợi trong giây lát'
    );
  }

  const setSorting = (field: keyof UsersListItem) => {
    const reversed = field === sortBy ? !isReverseSortDirection : false;

    setIsReverseSortDirection(reversed);
    setSortBy(field);

    if (usersListQuery.data) {
      setSortedData(
        table.sortData(usersListQuery.data, {
          sortBy: field,
          isReversed: reversed,
          search: searchValue,
        })
      );
    }
  };

  const searchChangeHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { value } = event.currentTarget;
    setSearchValue(value);
    if (usersListQuery.data) {
      setSortedData(
        table.sortData(usersListQuery.data, {
          sortBy: sortBy,
          isReversed: isReverseSortDirection,
          search: value,
        })
      );
    }
  };

  const deleteButtonClickHandler = (username: string) => {
    deleteUser.mutate(username, {
      onSuccess: () => {
        queryClient.invalidateQueries(['usersList']);
      },
    });
  };

  const rowsContent = sortedData?.map((row) => (
    <tr key={row.taiKhoan}>
      <td>{row.hoTen}</td>
      <td>{row.email}</td>
      <td>{row.taiKhoan}</td>
      <td>{row.matKhau}</td>
      <td>{row.soDT}</td>
      <td>{row.maLoaiNguoiDung}</td>
      <td>
        <Group noWrap={true}>
          <Button
            size='xs'
            onClick={() => deleteButtonClickHandler(row.taiKhoan)}
          >
            <IconTrash size={18} />
          </Button>
          <Button
            component={Link}
            to={`/admin/users/edit/${row.taiKhoan}`}
            size='xs'
          >
            <IconAdjustments size={18} />
          </Button>
        </Group>
      </td>
    </tr>
  ));

  return (
    <>
      {usersListQuery.isLoading && <Loader sx={{ alignSelf: 'center' }} />}
      {usersListQuery.data && (
        <>
          <TextInput
            placeholder='Tìm người dùng'
            icon={<IconSearch />}
            value={searchValue}
            onChange={searchChangeHandler}
            radius='md'
          />
          <Box className={clsx(classes.tableWrapper, 'custom-scrollbar')}>
            <Table
              className={classes.table}
              horizontalSpacing='md'
              verticalSpacing='xs'
            >
              <thead>
                <tr>
                  <AdminTableHeading
                    isSorted={sortBy === 'hoTen'}
                    isReversed={isReverseSortDirection}
                    onSort={() => setSorting('hoTen')}
                  >
                    Họ và tên
                  </AdminTableHeading>
                  <AdminTableHeading
                    isSorted={sortBy === 'email'}
                    isReversed={isReverseSortDirection}
                    onSort={() => setSorting('email')}
                  >
                    Email
                  </AdminTableHeading>
                  <AdminTableHeading
                    isSorted={sortBy === 'taiKhoan'}
                    isReversed={isReverseSortDirection}
                    onSort={() => setSorting('taiKhoan')}
                  >
                    Tài khoản
                  </AdminTableHeading>
                  <AdminTableHeading
                    isSorted={sortBy === 'matKhau'}
                    isReversed={isReverseSortDirection}
                    onSort={() => setSorting('matKhau')}
                  >
                    Mật khẩu
                  </AdminTableHeading>
                  <AdminTableHeading
                    isSorted={sortBy === 'soDT'}
                    isReversed={isReverseSortDirection}
                    onSort={() => setSorting('soDT')}
                  >
                    Số điện thoại
                  </AdminTableHeading>
                  <AdminTableHeading
                    isSorted={sortBy === 'maLoaiNguoiDung'}
                    isReversed={isReverseSortDirection}
                    onSort={() => setSorting('maLoaiNguoiDung')}
                  >
                    Mã loại người dùng
                  </AdminTableHeading>
                  <AdminTableNormalHeading>Thao tác</AdminTableNormalHeading>
                </tr>
              </thead>
              <tbody>
                {(rowsContent?.length ?? 0) > 0 ? (
                  rowsContent
                ) : (
                  <tr>
                    <td colSpan={7}>
                      <Text
                        weight='bold'
                        size='xl'
                        color='crusta'
                        align='center'
                      >
                        Không có dữ liệu
                      </Text>
                    </td>
                  </tr>
                )}
              </tbody>
            </Table>
          </Box>
        </>
      )}
    </>
  );
};

export default AdminUsersTable;
