import {
  Center,
  createStyles,
  Group,
  Text,
  UnstyledButton,
} from '@mantine/core';
import { IconChevronDown, IconChevronUp, IconSelector } from '@tabler/icons';
import React from 'react';

interface AdminUsersTableHeadingProps {
  children: React.ReactNode;
  isReversed: boolean;
  isSorted: boolean;
  onSort(): void;
}

const useStyles = createStyles((theme) => ({
  root: {
    whiteSpace: 'nowrap',
  },

  tableHeading: {
    padding: 0,
  },

  button: {
    width: '100%',
    padding: `${theme.spacing.xs}px ${theme.spacing.md}px`,
    borderRadius: theme.radius.md,

    '&:hover': {
      backgroundColor: theme.colors.crusta[9],
    },
  },

  icon: {
    width: 21,
    height: 21,
    borderRadius: 21,
  },
}));

const AdminUsersTableHeading = (props: AdminUsersTableHeadingProps) => {
  const { classes } = useStyles();
  const Icon = props.isSorted
    ? props.isReversed
      ? IconChevronUp
      : IconChevronDown
    : IconSelector;

  return (
    <th className={classes.root}>
      <UnstyledButton onClick={props.onSort} className={classes.button}>
        <Group position='apart' noWrap={true}>
          <Text weight={'bold'} color='crusta.0' size='sm'>
            {props.children}
          </Text>
          <Center className={classes.icon}>
            <Icon size={14} stroke={1.5} />
          </Center>
        </Group>
      </UnstyledButton>
    </th>
  );
};

export default AdminUsersTableHeading;
