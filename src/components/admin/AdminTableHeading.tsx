import {
  Center,
  clsx,
  createStyles,
  Group,
  Text,
  UnstyledButton,
} from '@mantine/core';
import { IconChevronDown, IconChevronUp, IconSelector } from '@tabler/icons';
import React from 'react';

interface AdminUsersTableHeadingProps {
  children: React.ReactNode;
  isReversed: boolean;
  isSorted: boolean;
  onSort(): void;
  canNotSort?: boolean;
}

const useStyles = createStyles((theme) => ({
  root: {
    whiteSpace: 'nowrap',
  },

  button: {
    width: '100%',
    paddingBlock: theme.spacing.xs,
    paddingInline: theme.spacing.md,
    borderRadius: theme.radius.md,

    '&:hover': {
      backgroundColor: theme.colors.crusta[9],
    },
  },

  canNotSort: {
    cursor: 'default',

    '&:hover, &:focus': {
      backgroundColor: theme.colors.mirage[3],
    },
  },

  icon: {
    width: 21,
    height: 21,
    borderRadius: 21,
  },
}));

const AdminTableHeading = (props: AdminUsersTableHeadingProps) => {
  const { classes } = useStyles();
  const Icon = props.isSorted
    ? props.isReversed
      ? IconChevronUp
      : IconChevronDown
    : IconSelector;

  return (
    <th style={{ paddingInline: 0 }} className={classes.root}>
      <UnstyledButton
        onClick={() => {
          if (!props.canNotSort) {
            props.onSort();
          }
        }}
        className={clsx(classes.button, props.canNotSort && classes.canNotSort)}
      >
        <Group position='apart' noWrap={true}>
          <Text weight={'bold'} color='crusta.0' size='sm'>
            {props.children}
          </Text>
          {!props.canNotSort && (
            <Center className={classes.icon}>
              <Icon size={14} stroke={1.5} />
            </Center>
          )}
        </Group>
      </UnstyledButton>
    </th>
  );
};

export default AdminTableHeading;
