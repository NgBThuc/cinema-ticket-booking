import { createStyles, Text } from '@mantine/core';
import React from 'react';

interface AdminUsersTableNormalHeadingProps {
  children: React.ReactNode;
}

const useStyles = createStyles((theme) => ({
  tableHeading: {
    paddingInline: theme.spacing.md,
    paddingBlock: theme.spacing.xs,
  },
}));

const AdminTableNormalHeading = (props: AdminUsersTableNormalHeadingProps) => {
  const { classes } = useStyles();

  return (
    <th className={classes.tableHeading}>
      <Text weight='bold' color='crusta.0'>
        {props.children}
      </Text>
    </th>
  );
};

export default AdminTableNormalHeading;
