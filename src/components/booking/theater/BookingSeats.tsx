import { clsx, createStyles, Stack, Text, Tooltip } from '@mantine/core';
import { ChangeEvent } from 'react';
import { useParams } from 'react-router-dom';
import { useSeatsList } from '../../../hooks/useBookingList';
import { useAppDispatch } from '../../../store';
import { bookingActions } from '../../../store/bookingSlice';

const useStyles = createStyles((theme) => ({
  seats: {
    display: 'grid',
    gap: theme.spacing.xs,
    rowGap: theme.spacing.md,
    gridTemplateColumns: 'repeat(16, min-content)',
    alignItems: 'center',
  },

  seatWrapper: {
    position: 'relative',
    width: '30px',
    height: '25px',
    cursor: 'pointer',

    '&:nth-of-type(16n + 4)': {
      marginRight: theme.spacing.xl,
    },

    '&:nth-of-type(16n + 12)': {
      marginRight: theme.spacing.xl,
    },
  },

  seat: {
    display: 'inline-block',
    background: '#626262',
    width: '100%',
    height: '100%',
    borderTopLeftRadius: '10px',
    borderTopRightRadius: '10px',
    transition: 'transform 300ms ease',

    '&.vip': {
      background: theme.colors.lynch[3],
    },

    '&.occupied': {
      cursor: 'default',
    },

    '&:not(.occupied):hover, &:not(.occupied):focus': {
      transform: 'scale(1.2)',
    },
  },

  seatCheckbox: {
    position: 'absolute',
    top: 0,
    left: 0,
    opacity: 0,
    cursor: 'pointer',
    height: 0,
    width: 0,

    '&:checked ~ span': {
      background: theme.colors.crusta[3],
    },

    '&:checked ~ span.occupied': {
      background: theme.colors.crusta[1],
    },
  },
}));

const BookingSeats = () => {
  const dispatch = useAppDispatch();
  const { classes } = useStyles();
  const params = useParams<{ scheduleId: string }>();
  const seatsListQuery = useSeatsList(params.scheduleId!);

  const checkboxChangeHandler = (
    event: ChangeEvent<HTMLInputElement>,
    seatId: number,
    seatPrice: number,
    seatName: string
  ) => {
    if (event.target.checked) {
      dispatch(
        bookingActions.selectSeatHandler({ seatId, seatPrice, seatName })
      );
    } else {
      dispatch(bookingActions.deselectSeatHandler(seatId));
    }
  };

  return (
    <div className={classes.seats}>
      {seatsListQuery.data?.map((seat) => {
        return (
          <Tooltip
            key={seat.maGhe}
            label={
              <Stack sx={{ gap: '0.1rem' }} align='center'>
                <Text color='crusta' weight='bold'>
                  {seat.tenGhe}
                </Text>
                {!seat.daDat && (
                  <Text color='crusta.1' weight='bold'>
                    {seat.loaiGhe === 'Thuong' ? 'Ghế thường' : 'Ghế VIP'}
                  </Text>
                )}
                {seat.daDat && (
                  <Text color='crusta.1' weight='bold'>
                    Ghế đã được mua
                  </Text>
                )}
              </Stack>
            }
          >
            <label className={classes.seatWrapper}>
              <input
                className={clsx(classes.seatCheckbox)}
                onChange={(event) => {
                  checkboxChangeHandler(
                    event,
                    seat.maGhe,
                    seat.giaVe,
                    seat.tenGhe
                  );
                }}
                checked={seat.daDat ? true : undefined}
                disabled={seat.daDat ? true : undefined}
                type='checkbox'
              ></input>
              <span
                className={clsx(
                  classes.seat,
                  seat.loaiGhe === 'Vip' && 'vip',
                  seat.daDat && 'occupied'
                )}
              ></span>
            </label>
          </Tooltip>
        );
      })}
    </div>
  );
};

export default BookingSeats;
