import { Box, Stack } from '@mantine/core';
import BookingInstruction from './BookingInstruction';
import BookingScreen from './BookingScreen';
import BookingSeats from './BookingSeats';

const BookingTheater = () => {
  return (
    <Stack
      sx={(theme) => ({
        width: '100%',
        overflowX: 'auto',
        overflowY: 'hidden',
        paddingBlock: theme.spacing.lg,
      })}
      className='custom-scrollbar'
    >
      <Box
        sx={{
          width: '800px',
          perspective: '400px',
          display: 'grid',
          placeItems: 'center',
          gap: '40px',
          marginInline: 'auto',
        }}
      >
        <BookingScreen />
        <BookingSeats />
        <BookingInstruction />
      </Box>
    </Stack>
  );
};

export default BookingTheater;
