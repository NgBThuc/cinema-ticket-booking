import { createStyles } from '@mantine/core';

const useStyles = createStyles((theme) => ({
  screen: {
    width: '80%',
    height: '150px',
    background: '#ffffff',
    transform: 'rotateX(-20deg) scale(1.1)',
    boxShadow: '0 3px 10px 2px',
  },
}));

const BookingScreen = () => {
  const { classes } = useStyles();

  return <div className={classes.screen}></div>;
};

export default BookingScreen;
