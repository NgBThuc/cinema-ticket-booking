import { clsx, createStyles, Group } from '@mantine/core';

const useStyles = createStyles((theme) => ({
  root: {
    border: `2px solid ${theme.colors.lynch[3]}`,
    paddingInline: theme.spacing.lg,
    paddingBlock: theme.spacing.md,
    borderRadius: theme.radius.md,
  },

  group: {
    alignItems: 'center',
    gap: theme.spacing.xs,
  },

  seat: {
    width: '30px',
    height: '25px',
    background: '#626262',
    borderTopLeftRadius: '10px',
    borderTopRightRadius: '10px',

    '&.occupied': {
      background: theme.colors.crusta[1],
    },

    '&.vip': {
      background: theme.colors.lynch[3],
    },
  },

  description: {
    fontWeight: 'bold',
  },
}));

const BookingInstruction = () => {
  const { classes } = useStyles();

  return (
    <Group spacing={'xl'} className={classes.root}>
      <Group className={classes.group}>
        <span className={clsx(classes.seat, 'occupied')}></span>
        <span className={classes.description}>Đã Đặt</span>
      </Group>
      <Group className={classes.group}>
        <span className={clsx(classes.seat, 'vip')}></span>
        <span className={classes.description}>Ghế Vip</span>
      </Group>
      <Group className={classes.group}>
        <span className={classes.seat}></span>
        <span className={classes.description}>Ghế Thường</span>
      </Group>
    </Group>
  );
};

export default BookingInstruction;
