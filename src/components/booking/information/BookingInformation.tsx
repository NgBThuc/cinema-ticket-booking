import { Button, createStyles, Stack, Title } from '@mantine/core';
import { cleanNotifications } from '@mantine/notifications';
import { useMutation, useQueryClient } from '@tanstack/react-query';
import _ from 'lodash';
import { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { useNavigate, useParams } from 'react-router-dom';
import { useBookingInformation } from '../../../hooks/useBookingList';
import { bookingServices } from '../../../services/bookingServices';
import { RootState, useAppDispatch } from '../../../store';
import { bookingActions } from '../../../store/bookingSlice';
import { Booking } from '../../../types/apis/booking';
import {
  openConfirmBookingModal,
  openRequestLoginModal,
} from '../../../utils/modal';
import { notification } from '../../../utils/notification';
import Loading from '../../ui/Loading';
import BookingInformationItem from './BookingInformationItem';

const useStyles = createStyles((theme) => ({
  infoContainer: {
    width: '100%',
  },
}));

const BookingInformation = () => {
  const { classes } = useStyles();
  const navigate = useNavigate();
  const params = useParams<{ scheduleId: string }>();
  const dispatch = useAppDispatch();
  const isLoggedIn = useSelector((state: RootState) => state.auth.isLoggedIn);
  const bookingInformationQuery = useBookingInformation(params.scheduleId!);
  const queryClient = useQueryClient();
  const bookingList = useSelector(
    (state: RootState) => state.booking.bookingList
  );
  const confirmBookingRequest = useMutation(
    (bookingData: Booking) => {
      return bookingServices.confirm(bookingData);
    },
    {
      onSuccess: (bookingResponse) => {
        navigate('/', { replace: true });
        cleanNotifications();
        notification.success(
          'Đặt vé thành công',
          'Vui lòng kiểm tra thông tin đặt vé tại trang cá nhân'
        );
        dispatch(bookingActions.resetBookingList());
        queryClient.invalidateQueries(['bookingList', params.scheduleId]);
      },
      onError: () => {
        notification.error('Đặt vé không thành công', 'Vui lòng thử lại');
      },
    }
  );

  useEffect(() => {
    dispatch(bookingActions.resetBookingList());
    if (confirmBookingRequest.isLoading) {
      notification.loading(
        'Tiến hành đặt vé',
        'Quý khách vui lòng chờ trong giây lát'
      );
    }
  }, [confirmBookingRequest.isLoading, dispatch]);

  if (bookingInformationQuery.isLoading) {
    return <Loading />;
  }

  const seatInfoData =
    bookingList.length !== 0
      ? _.map(bookingList, 'seatName').join(', ')
      : 'Bạn chưa chọn ghế';

  const totalInfoData = new Intl.NumberFormat('vi-VN').format(
    _.sumBy(bookingList, 'seatPrice')
  );

  const bookingConfirmHandler = () => {
    const bookingData: Booking = {
      maLichChieu: +params.scheduleId!,
      danhSachVe: _.map(bookingList, (bookingItem) => {
        return {
          maGhe: bookingItem.seatId,
          giaVe: bookingItem.seatPrice,
        };
      }),
    };
    confirmBookingRequest.mutate(bookingData);
  };

  const bookingInformationContent: {
    title: string;
    data: string | undefined;
  }[] = [
    {
      title: 'Tên phim',
      data: bookingInformationQuery.data?.movie,
    },
    {
      title: 'Cụm rạp',
      data: bookingInformationQuery.data?.theater,
    },
    {
      title: 'Địa chỉ',
      data: bookingInformationQuery.data?.address,
    },
    {
      title: 'Rạp',
      data: bookingInformationQuery.data?.theaterNum,
    },
    {
      title: 'Ngày chiếu',
      data: bookingInformationQuery.data?.date,
    },
    {
      title: 'Giờ chiếu',
      data: bookingInformationQuery.data?.time,
    },
    {
      title: 'Số ghế',
      data: seatInfoData,
    },
    {
      title: 'Tổng tiền',
      data: totalInfoData,
    },
  ];

  return (
    <Stack align={'center'} style={{ width: '100%' }} spacing='xl'>
      <Title variant='gradient' order={1}>
        Thông tin đặt vé
      </Title>

      <Stack spacing={'lg'} className={classes.infoContainer}>
        {bookingInformationContent.map((informationItem) => {
          return (
            <BookingInformationItem
              key={informationItem.title}
              title={informationItem.title}
              data={informationItem.data}
            />
          );
        })}
      </Stack>

      <Button
        variant='filled'
        size='xl'
        radius={'lg'}
        sx={(theme) => ({
          border: 'none',
          [theme.fn.largerThan('lg')]: {
            width: '80%',
          },
        })}
        disabled={bookingList.length === 0}
        onClick={
          isLoggedIn
            ? openConfirmBookingModal.bind(
                null,
                totalInfoData,
                bookingConfirmHandler
              )
            : openRequestLoginModal
        }
      >
        {bookingList.length === 0 ? 'Vui lòng chọn ghế' : 'Xác nhận đặt vé'}
      </Button>
    </Stack>
  );
};

export default BookingInformation;
