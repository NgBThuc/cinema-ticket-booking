import { createStyles, Stack, Text } from '@mantine/core';

interface BookingInformationProps {
  title: string;
  data: string | undefined;
}

const useStyles = createStyles((theme) => ({
  root: {
    flexDirection: 'column',
    justifyContent: 'center',
    gap: '0.5rem',

    [theme.fn.largerThan('lg')]: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'start',
    },
  },

  dataTitle: {
    width: '100%',

    [theme.fn.largerThan('lg')]: {
      width: '30%',
    },
  },

  dataText: {
    textAlign: 'center',
    width: '100%',

    [theme.fn.largerThan('lg')]: {
      textAlign: 'right',
      width: '70%',
    },
  },
}));

const BookingInformationItem = (props: BookingInformationProps) => {
  const { classes } = useStyles();

  return (
    <Stack className={classes.root} align={'center'}>
      <Text size={'xl'} color={'crusta'} weight='bold'>
        {props.title}
      </Text>
      <Text className={classes.dataText} size={'xl'}>
        {props.data}
      </Text>
    </Stack>
  );
};

export default BookingInformationItem;
