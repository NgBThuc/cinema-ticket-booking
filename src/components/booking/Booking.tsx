import { Box, Stack } from '@mantine/core';
import BookingInformation from './information/BookingInformation';
import { default as BookingTheater } from './theater/BookingTheater';

const Booking = () => {
  return (
    <Box
      sx={{
        paddingBlock: '5rem',
      }}
    >
      <Stack
        sx={(theme) => ({
          gap: theme.spacing.xl,

          [theme.fn.largerThan('lg')]: {
            flexDirection: 'row',
            alignItems: 'start',
          },
        })}
        align={'center'}
        className='container'
      >
        <BookingTheater />
        <BookingInformation />
      </Stack>
    </Box>
  );
};

export default Booking;
