import { createStyles } from '@mantine/core';

interface VideoEmbedProps {
  embedLink: string;
}

const useStyles = createStyles((theme) => ({
  video: {
    overflow: 'hidden',
    paddingBottom: '56.25%',
    position: 'relative',
    height: 0,
    borderRadius: theme.radius.md,

    '& iframe': {
      left: 0,
      top: 0,
      height: '100%',
      width: '100%',
      position: 'absolute',
    },
  },
}));

const getYoutubeVideoId = (url: string) => {
  const regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|&v=)([^#&?]*).*/;
  const match = url.match(regExp);

  return match && match[2].length === 11 ? match[2] : null;
};

const VideoEmbed = (props: VideoEmbedProps) => {
  const { classes } = useStyles();

  return (
    <div className={classes.video}>
      <iframe
        width='853'
        height='480'
        src={`https://www.youtube.com/embed/${getYoutubeVideoId(
          props.embedLink
        )}`}
        frameBorder='0'
        allow='accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture'
        allowFullScreen
        title='Embedded youtube'
      />
    </div>
  );
};

export default VideoEmbed;
