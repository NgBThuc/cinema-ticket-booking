import { Box, LoadingOverlay } from '@mantine/core';

const Loading = () => {
  return (
    <Box
      sx={(theme) => ({
        width: '100vw',
        height: '100vh',
        position: 'fixed',
        top: 0,
        left: 0,
        backgroundColor: theme.colors.mirage[3],
        zIndex: 10000,
      })}
    >
      <LoadingOverlay visible={true} overlayBlur={2} />
    </Box>
  );
};

export default Loading;
