import { Affix, Button, Transition } from '@mantine/core';
import { useWindowScroll } from '@mantine/hooks';
import { IconArrowUp } from '@tabler/icons';

const ScrollToTopButton = () => {
  const [scroll, scrollTo] = useWindowScroll();

  const scrollToTopHandler = (): void => {
    scrollTo({ y: 0 });
  };

  return (
    <Affix position={{ bottom: 20, right: 20 }}>
      <Transition transition='slide-up' mounted={scroll.y > 0}>
        {(transitionStyles) => {
          return (
            <Button
              leftIcon={<IconArrowUp />}
              style={transitionStyles}
              onClick={scrollToTopHandler}
            >
              Lên Đầu Trang
            </Button>
          );
        }}
      </Transition>
    </Affix>
  );
};

export default ScrollToTopButton;
