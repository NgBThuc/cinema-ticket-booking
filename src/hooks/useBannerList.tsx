import { useQuery } from '@tanstack/react-query';
import { movieServices } from '../services/movieServices';
import { moveToErrorPage } from '../utils/error';

export function useBannerList() {
  return useQuery(['bannerList'], () => movieServices.getBannerList(), {
    onError: moveToErrorPage.error500,
  });
}
