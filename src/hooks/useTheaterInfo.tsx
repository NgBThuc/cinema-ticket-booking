import { useQuery } from '@tanstack/react-query';
import { theaterServices } from '../services/theaterServices';
import { moveToErrorPage } from '../utils/error';

export const useTheaterSystemInfo = () => {
  return useQuery(['theaterSystemInfo'], () =>
    theaterServices.getTheaterSystemInfo()
  );
};

export const useTheaterGroupInfo = (theaterSystemId: string) => {
  return useQuery(
    ['theaterGroupInfo', theaterSystemId],
    () => theaterServices.getTheaterGroupInfo(theaterSystemId),
    {
      enabled: !!theaterSystemId,
      onError: moveToErrorPage.error500,
    }
  );
};
