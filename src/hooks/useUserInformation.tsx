import { useQuery } from '@tanstack/react-query';
import { userServices } from '../services/userServices';
import { moveToErrorPage } from '../utils/error';

export function useUserInformation() {
  return useQuery(['userInfo'], () => userServices.getInfo(), {
    onError: moveToErrorPage.error500,
    staleTime: 0,
    refetchOnMount: true,
  });
}
