import { useQuery } from '@tanstack/react-query';
import { userServices } from '../services/userServices';
import { moveToErrorPage } from '../utils/error';

export const useUsersList = () => {
  return useQuery(['usersList'], () => userServices.getUserList(), {
    staleTime: 0,
    refetchOnMount: true,
    onError: () => moveToErrorPage.error500,
  });
};
