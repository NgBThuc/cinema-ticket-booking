import { useQuery } from '@tanstack/react-query';
import { bookingServices } from '../services/bookingServices';
import { BookingList } from '../types/apis/bookingList';
import { moveToErrorPage } from '../utils/error';

function selectBookingInfoData(data: BookingList) {
  let bookingInfo = data.thongTinPhim;
  return {
    movie: bookingInfo.tenPhim,
    theater: bookingInfo.tenCumRap,
    bookingId: bookingInfo.maLichChieu.toString(),
    time: bookingInfo.gioChieu,
    date: bookingInfo.ngayChieu,
    address: bookingInfo.diaChi,
    theaterNum: bookingInfo.tenRap,
  };
}

function selectSeatsList(data: BookingList) {
  return data.danhSachGhe;
}

export function useBookingList<T = BookingList>(
  bookingId: string,
  select?: (data: BookingList) => T,
  onSuccess?: () => void
) {
  return useQuery(
    ['bookingList', bookingId],
    () => bookingServices.getBookingList(bookingId),
    {
      enabled: !!bookingId,
      select,
      onSuccess,
      onError: moveToErrorPage.error500,
    }
  );
}

export function useBookingInformation(
  bookingId: string,
  onSuccess?: () => void
) {
  return useBookingList(bookingId, selectBookingInfoData, onSuccess);
}

export function useSeatsList(bookingId: string) {
  return useBookingList(bookingId, selectSeatsList);
}
