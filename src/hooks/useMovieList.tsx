import { useQuery } from '@tanstack/react-query';
import _ from 'lodash';
import { movieServices } from '../services/movieServices';
import { MovieList } from '../types/apis/moviesList';
import { moveToErrorPage } from '../utils/error';

function selectMoviesListData(data: MovieList) {
  const currentMovies = _.filter(data, { dangChieu: true });
  return _.chunk(currentMovies, 6);
}

function selectPlayingMovies(data: MovieList) {
  return _.map(_.filter(data, { dangChieu: true }), (movie) => {
    return {
      label: movie.tenPhim,
      value: movie.maPhim.toString(),
    };
  });
}

export function useAdminMoviesList() {
  return useQuery(['moviesList'], () => movieServices.getMoviesList(), {
    staleTime: 0,
    refetchOnMount: true,
  });
}

export function useMoviesList() {
  return useQuery(['movieList'], () => movieServices.getMoviesList(), {
    select: selectMoviesListData,
    onError: moveToErrorPage.error500,
  });
}

export function usePlayingMovies() {
  return useQuery(['movieList'], () => movieServices.getMoviesList(), {
    select: selectPlayingMovies,
    onError: moveToErrorPage.error500,
  });
}
