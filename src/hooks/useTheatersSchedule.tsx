import { useQuery } from '@tanstack/react-query';
import { movieServices } from '../services/movieServices';
import { moveToErrorPage } from '../utils/error';

export function useTheatersSchedule() {
  return useQuery(
    ['movieScheduleByTheater'],
    () => movieServices.getMovieScheduleByTheater(),
    {
      onError: moveToErrorPage.error500,
    }
  );
}
