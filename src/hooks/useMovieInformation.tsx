import { useQuery } from '@tanstack/react-query';
import { movieServices } from '../services/movieServices';
import { moveToErrorPage } from '../utils/error';

export function useMovieInformation(movieId: string) {
  return useQuery(
    ['movieInformation', movieId],
    () => movieServices.getMovieInformation(movieId),
    {
      enabled: !!movieId,
      onError: moveToErrorPage.error500,
    }
  );
}
