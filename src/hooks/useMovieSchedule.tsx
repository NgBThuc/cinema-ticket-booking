import { useQuery } from '@tanstack/react-query';
import dayjs from 'dayjs';
import _ from 'lodash';
import { useCallback } from 'react';
import { movieServices } from '../services/movieServices';
import { MovieSchedule } from '../types/apis/movieSchedule';
import { moveToErrorPage } from '../utils/error';

function selectTheaterData(data: MovieSchedule) {
  let theatersSystem = data.heThongRapChieu;

  let theatersSelectData = _.flatMap(theatersSystem, (theater) => {
    return _(theater.cumRapChieu)
      .map((branch) => ({ value: branch.maCumRap, label: branch.tenCumRap }))
      .value();
  });

  return theatersSelectData;
}

function selectScheduleData(data: MovieSchedule, theaterId: string) {
  return _.chain(data.heThongRapChieu)
    .flatMap((theater) => theater.cumRapChieu)
    .find((theaterBranch) => theaterBranch.maCumRap === theaterId)
    .get('lichChieuPhim')
    .map((schedule) => {
      return {
        value: schedule.maLichChieu,
        label: `${dayjs(schedule.ngayChieuGioChieu).format(
          'DD/MM/YYYY'
        )} - ${dayjs(schedule.ngayChieuGioChieu).format('HH:mm')}`,
      };
    })
    .value();
}

export function useMovieSchedule<T = MovieSchedule>(
  movieId: string,
  select?: (data: MovieSchedule) => T
) {
  return useQuery(
    ['movieSchedule', movieId],
    () => movieServices.getMovieSchedule(movieId),
    {
      enabled: !!movieId,
      select,
      onError: moveToErrorPage.error500,
    }
  );
}

export function useSelectTheater(movieId: string) {
  return useMovieSchedule(movieId, selectTheaterData);
}

export function useSelectSchedule(movieId: string, theaterId: string) {
  return useMovieSchedule(
    movieId,
    useCallback(
      (data: MovieSchedule) => selectScheduleData(data, theaterId),
      [theaterId]
    )
  );
}
