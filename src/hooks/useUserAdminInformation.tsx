import { useQuery } from '@tanstack/react-query';
import { userServices } from '../services/userServices';
import { moveToErrorPage } from '../utils/error';

export const useUserAdminInformation = (username: string) => {
  return useQuery(
    ['userAdminInfo', username],
    () => userServices.getUserAdminInfo(username),
    {
      enabled: !!username,
      onError: moveToErrorPage.error500,
    }
  );
};
