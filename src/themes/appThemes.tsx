import { ButtonStylesParams, MantineThemeOverride } from '@mantine/core';

export const appTheme: MantineThemeOverride = {
  colorScheme: 'dark',
  respectReducedMotion: false,
  cursorType: 'pointer',
  colors: {
    crusta: [
      '#E7C7B4',
      '#E7AF8F',
      '#EE9866',
      '#FF8036',
      '#E8732E',
      '#CD682D',
      '#AA6137',
      '#8F5A3C',
      '#79533D',
      '#684C3D',
    ],
    mirage: [
      '#303744',
      '#293140',
      '#232C3D',
      '#1D273A',
      '#1D2432',
      '#1C212B',
      '#1A1E25',
      '#191B20',
      '#17191C',
      '#151619',
    ],
    lynch: [
      '#9BA1AE',
      '#8891A3',
      '#75819B',
      '#637394',
      '#5E6981',
      '#576071',
      '#515763',
      '#4A4F58',
      '#44474E',
      '#3E4045',
    ],
  },
  defaultGradient: { from: '#ff8800', to: '#ff0000' },
  primaryColor: 'crusta',
  primaryShade: 3,
  loader: 'oval',
  breakpoints: {
    xs: 576,
    sm: 768,
    md: 992,
    lg: 1200,
    xl: 1400,
  },
  fontFamily: 'Open Sans, sans-serif',
  components: {
    Button: {
      styles: (theme, params: ButtonStylesParams) => ({
        root: {
          transition: 'all 300ms ease',
          border: `2px solid ${theme.colors.crusta[3]}`,
          color: '#ffffff',

          '&:hover': {
            backgroundColor:
              params.variant === 'outline'
                ? `${theme.colors.crusta[3]}`
                : `${theme.colors.crusta[5]}`,
          },
        },
      }),
    },
    Anchor: {
      styles: (theme) => ({
        root: {
          color: '#ffffff',
          transition: 'all 300ms ease',
          fontWeight: 600,

          '&:hover': {
            textDecoration: 'none',
            color: theme.colors.crusta[3],
          },
        },
      }),
    },
    Image: {
      styles: {
        figure: {
          height: '100%',
        },
        imageWrapper: {
          height: '100%',
        },
        image: {
          height: 'auto',
          maxWidth: '100%',
          objectFit: 'cover',
        },
      },
    },
    Carousel: {
      styles: (theme) => ({
        root: {
          borderRadius: theme.radius.lg,
          cursor: 'move',
        },
        indicator: {
          with: 12,
          height: 4,
          transition: 'width 250ms ease',

          '&[data-active]': {
            width: 40,
          },
        },
      }),
    },
    Grid: {
      styles: (theme) => ({
        root: {
          margin: 0,
        },
      }),
    },
  },
};
