import { Global } from '@mantine/core';

const GlobalStyles = () => {
  return (
    <Global
      styles={(theme) => ({
        body: {
          backgroundColor: theme.colors.mirage[3],
        },

        '.container': {
          width: '100%',
          marginInline: 'auto',
          paddingInline: '1rem',

          [theme.fn.largerThan('xs')]: {
            width: '540px',
          },

          [theme.fn.largerThan('sm')]: {
            width: '720px',
          },

          [theme.fn.largerThan('md')]: {
            width: '960px',
          },

          [theme.fn.largerThan('lg')]: {
            width: '1140px',
          },

          [theme.fn.largerThan('xl')]: {
            width: '1320px',
          },
        },

        '.custom-scrollbar': {
          '::-webkit-scrollbar': {
            width: '6px',
            height: '6px',

            [theme.fn.smallerThan('md')]: {
              display: 'none',
            },
          },

          '::-webkit-scrollbar-track': {
            background: 'rgb(55, 58, 64)',
          },

          '::-webkit-scrollbar-thumb': {
            background: theme.colors.crusta[6],
            borderRadius: theme.radius.lg,
          },

          '::-webkit-scrollbar-thumb:hover': {
            background: theme.colors.crusta[4],
          },
        },
      })}
    />
  );
};

export default GlobalStyles;
