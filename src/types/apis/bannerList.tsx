export type BannerList = BannerItem[];

export interface BannerItem {
  maBanner: number;
  maPhim: number;
  hinhAnh: string;
}
