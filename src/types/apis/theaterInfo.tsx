export interface TheaterSystemInfo {
  maHeThongRap: string;
  tenHeThongRap: string;
  biDanh: string;
  logo: string;
}

export interface TheaterGroupInfo {
  maCumRap: string;
  tenCumRap: string;
  diaChi: string;
  danhSachRap: TheaterInfo[];
}

export interface TheaterInfo {
  maRap: string;
  tenRap: string;
}
