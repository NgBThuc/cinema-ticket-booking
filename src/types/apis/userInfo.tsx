export interface UserInfo {
  taiKhoan: string;
  matKhau: string;
  hoTen: string;
  email: string;
  soDT: string;
  maNhom: string;
  maLoaiNguoiDung: string;
  loaiNguoiDung: UserType;
  thongTinDatVe: BookingInfo[];
}

export interface UserType {
  maLoaiNguoiDung: string;
  tenLoai: string;
}

export interface BookingInfo {
  danhSachGhe: SeatsList[];
  maVe: number;
  ngayDat: string;
  tenPhim: string;
  hinhAnh: string;
  giaVe: number;
  thoiLuongPhim: number;
}

export interface SeatsList {
  maHeThongRap: string;
  tenHeThongRap: string;
  maCumRap: string;
  tenCumRap: string;
  maRap: number;
  tenRap: string;
  maGhe: number;
  tenGhe: string;
}
