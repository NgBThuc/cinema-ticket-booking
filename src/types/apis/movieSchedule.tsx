export interface MovieSchedule {
  heThongRapChieu: TheaterSystem[];
  maPhim: number;
  tenPhim: string;
  biDanh: string;
  trailer: string;
  hinhAnh: string;
  moTa: string;
  maNhom: string;
  hot: boolean;
  dangChieu: boolean;
  sapChieu: boolean;
  ngayKhoiChieu: string;
  danhGia: number;
}

export interface TheaterSystem {
  cumRapChieu: TheaterBranch[];
  maHeThongRap: string;
  tenHeThongRap: string;
  logo: string;
}

export interface TheaterBranch {
  lichChieuPhim: MovieScheduleDetail[];
  maCumRap: string;
  tenCumRap: string;
  hinhAnh: string;
  diaChi: string;
}

export interface MovieScheduleDetail {
  maLichChieu: string;
  maRap: string;
  tenRap: string;
  ngayChieuGioChieu: string;
  giaVe: number;
  thoiLuong: number;
}
