export type TheaterSchedule = TheaterGroup[];

export interface TheaterGroup {
  lstCumRap: TheaterBranch[];
  maHeThongRap: string;
  tenHeThongRap: string;
  logo: string;
  mahom: string;
}

export interface TheaterBranch {
  danhSachPhim: MovieList[];
  maCumRap: string;
  tenCumRap: string;
  hinhAnh: string;
  diaChi: string;
}

export interface MovieList {
  lstLichChieuTheoPhim: MovieScheduleByMovie[];
  maPhim: number;
  tenPhim: string;
  hinhAnh: string;
  hot: boolean;
  dangChieu: boolean;
  sapChieu: boolean;
}

export interface MovieScheduleByMovie {
  maLichChieu: number;
  maRap: string;
  tenRap: string;
  ngayChieuGioChieu: string;
  giaVe: number;
}
