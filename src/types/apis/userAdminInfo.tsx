export interface UserAdminInfo {
  taiKhoan: string;
  matKhau: string;
  hoTen: string;
  email: string;
  soDT: string;
  maNhom: string;
  maLoaiNguoiDung: string;
  loaiNguoiDung: UserType;
  thongTinDatVe: any[];
}

export interface UserType {
  maLoaiNguoiDung: string;
  tenLoai: string;
}
