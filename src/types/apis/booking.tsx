export interface Booking {
  maLichChieu: number;
  danhSachVe: BookingList[];
}

export interface BookingList {
  maGhe: number;
  giaVe: number;
}
