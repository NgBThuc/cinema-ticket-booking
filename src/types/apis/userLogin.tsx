export interface LoginData {
  taiKhoan: string;
  matKhau: string;
}

export interface LoginResponse {
  taiKhoan: string;
  hoTen: string;
  email: string;
  soDT: string;
  maNhom: string;
  maLoaiNguoiDung: string;
  accessToken: string;
}
