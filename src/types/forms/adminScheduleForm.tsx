export interface AdminScheduleForm {
  [key: string]: string | Date | number | undefined;
  theaterSystem: string;
  theaterGroup: string;
  showDate: Date;
  showTime: Date;
  ticketPrice: number;
}
