export interface AdminMoviesFormValues {
  [key: string]: string | boolean | number | File | Date | null;
  tenPhim: string;
  trailer: string;
  moTa: string;
  maNhom: string;
  ngayKhoiChieu: Date;
  sapChieu: boolean;
  dangChieu: boolean;
  hot: boolean;
  danhGia: number;
  hinhAnh: File | null | string;
}

export interface ScheduleData {
  maPhim: number;
  ngayChieuGioChieu: string;
  maRap: string;
  giaVe: number;
}
